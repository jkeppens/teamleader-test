# Team Leader : Discounts

*Author:* Jeroen Keppens <jeroen.keppens@gmail.com>

[![Build Status](https://github.com/mezzio/mezzio-skeleton/actions/workflows/continuous-integration.yml/badge.svg)](https://github.com/mezzio/mezzio-skeleton/actions/workflows/continuous-integration.yml)

## Getting Started

The application runs very simple using the built-in webserver. Since PHP8.1 features are being used, this requires a local php 8.1 configuration.

Start by installing the dependencies, then run:

```bash
$ composer install
```

To start the server, you can run:

```bash
$ composer serve
```

The webservice will be available on http://localhost:8888.

## Configuration

### Database

For ease of use, I implemented a simple file based system. The configuration
of the database paths is done in the file

`config/autoload/database.global.php`

I implemented 2 types of database files, one in json format, one in php format. 
All could have done in JSON, but quite frankly I wanted to configure the actions 
and conditions in a more convenient way. The php array file allows me to have
classes defined using imports and `::class` notation, which makes it easier and 
also less error prone. Since only developers would ever update them when adding
new classes, it is an easy alternative for read-only data.

In the config file, you will find the option for files and paths under json. 
The files are simply json files with multiple records in 1 file, the paths just
define a path where files per record can be found (and it contains for instance 
the 3 sample orders).

In a production application, we would of course use a proper database. This can
be easily implemented without impacting the application layers since the services,
commands and queries expect an implementation of a repository interface, and the 
configuration to use a file or db repository implementation is purely done on DI
level, so it can be easily swapped out.

See `config/autoload/dependencies.global.php` for application
level dependency injection config:

```php
'aliases' => [
    CustomerRepository::class => CustomerFileRepository::class,
    OrderRepository::class => OrderFileRepository::class,
    ProductRepository::class => ProductFileRepository::class,
    ActionRepository::class => ActionFileRepository::class,
    ConditionRepository::class => ConditionFileRepository::class,
    RuleRepository::class => RuleFileRepository::class,
],
```

The Repository could also be a REST repository reading for instance the
customers, orders, products from another service. The application layer
(or other layers for that matter) do not care where the data comes from.

### Dependency Injection

As already specified, the "Application"-level DI happens in the file
`config/autoload/dependencies.global.php`.

Each component however has a `ConfigProvider` class to configure "fixed"
component level dependency injection / factories.

For the File repositories for instance, the factory for creating the file
repository is in the ConfigProvider (this could have been a factory class),
while the alias mapping the interface to the implementation is done on
class level.

### Discount Strategies

The 3rd application level config contains the "discount strategies".

```php
return [
    'discount-strategy' => [
        'order' => DiscountClashResolvementEnum::UseHighestValue,
        'order-items' => DiscountClashResolvementEnum::UseHighestValue,
        'free-items' => DiscountClashResolvementEnum::AddDiscount,
    ],
];
```

What this basically specifies is what to do when a discount is added for
an order or item that already received a discount (competing rules).

There are currently 3 types of discounts, and you can define a different
strategy for each:

* **order** - a discount over the whole order
* **order-items** - a discount over one specific order item
* **free-items** - a free product given

You can assign one of the `DiscountClashResolvementEnum` values to it:

* **UseHighestValue** - use the highest discount of the two
* **UseLowestValue** - use the lowest discount of the two
* **UseFirstDiscount** - use the first discount applied
* **UseLastDiscount** - use the last discount applied
* **AddDiscount** - add discounts or quantities

This was not in the scope of the task, but it was something I ran against
testing the scenarios, so I added it for completeness.

## Discounts component

### Concept

The `CalculateDiscountService` will receive an order aggregate and calculate
the discounts based on a given ruleset.

A rule has a condition that needs to be fulfilled and an action that happens 
as a result.

A condition is specified by a condition class (implementing `ConditionInterface`)
and potentially receiving configuration parameters. The same condition class
can thus be used for multiple scenario's, for instance in one rule the customer revenue
check can be for more than 500 euro, in another rule this check can be for 1000 euro.
This is configured in the condition-params in the rule.

A condition will result in one or more applicable objects. The service will then
apply the action (with action params configuration) to each of those applicable objects and modify the `DiscountsAggregate`
object.

The `DiscountsAggregate` accepts discounts for the order, discounts for individual items or 
for free items and decides to add it or to resolve the configured strategy based on the
configuration.

It is very easy to add new Conditions or Action types and make this system very extendable.

At the moment there is no "shipping discount" but this could be added to the discount aggregate, a special
`ShippingDiscountEntity` could be created after which an action could provide for instance reduced
or free shipping.

I added some extra conditions / actions already, this is the full list:

#### Conditions

* **RevenueBetween** - Customer Revenue is between two values
* **RevenueHigherThan** - Customer Revenue is higher than or equal to a value
* **RevenueLowerThan** - Customer Revenue is lower than or equal to a value
* **RevenueBetween** - Customer joined between two specified date
* **CustomerSinceAfter** - Customer joined on or after a specific date
* **CustomerSinceBefore** - Customer joined on or before a specific date
* **ForEveryProductInCategoryWithQuantityOfItemsBetween** - Finds all products in a category with a quantity between 2 values
* **ForEveryProductInCategoryWithQuantityOfItemsExactly** - Finds all products in a category with a quantity equal to a value
* **ForEveryProductInCategoryWithQuantityOfItemsHigherThan** - Finds all products in a category with a quantity higher than a value
* **ForEveryProductInCategoryWithQuantityOfItemsLowerThan** - Finds all products in a category with a quantity lower than a value
* **ForNumberOfProductsInSameCategoryBetween** - Loads all order items for a category if the amount of items between 2 values (example: between 2 and 5 different products bought from tools category)
* **ForNumberOfProductsInSameCategoryExactly** - Loads all order items for a category if the amount of items equal to a value (example: 2 different products bought from tools category)
* **ForNumberOfProductsInSameCategoryHigherThan** - Loads all order items for a category if the amount of items higher than or equal to a value (example: at least 2 different products bought from tools category)
* **ForNumberOfProductsInSameCategoryLowerThan** - Loads all order items for a category if the amount of items lower than or equal to a value (example: at most 2 different products bought from tools category)
* **OrderTotalBetween** - Order Total Amount is Between 2 values
* **OrderTotalHigherThan** - Order Total Amount is higher than or equal to a value
* **OrderTotalLowerThan** - Order Total Amount is lower than or equal to a value

#### Actions

* **AddFreeItems** - add one or more free items to the order
* **AddOrderItemPercentageDiscount** - add a percentage based discount to an order item
* **AddOrderItemValueDiscount** - add a fixed amount discount to an order item
* **AddOrderPercentageDiscount** - add a percentage based discount to an order
* **AddOrderValueDiscount** - add a fixed amount discount to an order
* **AddOrderItemPercentageDiscountOnOneOfTheItems** - add a percentage based discount to one of the order items in a selection

### Rest End-Points

In the root of the project is a file `postman_collection.json` that can be imported into postman for sending data to the API endpoints.

The application currently defines following rest end-points:

#### Calculate a discount

`POST http://localhost:8888/api/calculate-discount`

This calculates the discounts for an order structure provided.

Sample request:

```json
{
  "id": "1",
  "customer-id": "2",
  "items": [
    {
      "product-id": "B102",
      "quantity": "5",
      "unit-price": "4.99",
      "total": "24.95"
    }
  ],
  "total": "24.95"
}
```

Sample response:

```json
{
    "order": {
        "discount": 2.5,
        "percentage": 10,
        "reason": "when-customer-revenue-over-1000-give-10-percent-discount-on-order"
    },
    "free-items": [
        {
            "product-id": "B102",
            "quantity": 1,
            "reason": "for-every-5-items-bought-for-products-in-category-switches-give-1-item-free"
        }
    ]
}
```

#### Calculates Discount for provided Order ID

`POST http://localhost:8888/api/calculate-discount-for-order-id`

This accepts an `order_id` as post parameter and will load the order and 
calculate the discounts for it.

Sample request:

`order_id=2`

Sample response:

```json
{
    "order": {
        "discount": 2.5,
        "percentage": 10,
        "reason": "when-customer-revenue-over-1000-give-10-percent-discount-on-order"
    },
    "free-items": [
        {
            "product-id": "B102",
            "quantity": 1,
            "reason": "for-every-5-items-bought-for-products-in-category-switches-give-1-item-free"
        }
    ]
}
```

#### List all current discount rules

`GET http://localhost:8888/api/discounts/rules`

Show all rules (no filtering at the moment):

Example response:

```json
[
  {
    "id": 1,
    "title": "when-customer-revenue-over-1000-give-10-percent-discount-on-order",
    "condition": "customer-revenue-higher-than",
    "condition-params": {
      "amount": 1000
    },
    "action": "add-order-percentage-discount",
    "action-params": {
      "percentage": 10
    }
  },
  {
    "id": 2,
    "title": "for-every-5-items-bought-for-products-in-category-switches-give-1-item-free",
    "condition": "for-every-product-in-category-with-quantity-of-items-higher-than",
    "condition-params": {
      "categoryId": 2,
      "quantity": 5
    },
    "action": "add-free-items",
    "action-params": {
      "quantity": 1,
      "quantityPerChunkOf": 5
    }
  },
  {
    "id": 4,
    "title": "for-2-or-more-products-boughts-in-tools-give-20-percent-discount-on-cheapest-product",
    "condition": "for-number-of-products-in-same-category-higher-than",
    "condition-params": {
      "categoryId": 1,
      "quantity": 2
    },
    "action": "add-order-item-percentage-discount-on-one-of-the-items",
    "action-params": {
      "checkOn": "unit_price",
      "checkFor": "min",
      "percentage": 20
    }
  }
]
```

#### Configure a new rule

`POST http://localhost:8888/api/discounts/rules`

Example request:

```json
{
  "title": "order-total-higher-than-500-euro",
  "condition": "order-total-higher-than",
  "condition-params": {
    "amount": 500
  },
  "action": "add-order-item-percentage-discount",
  "action-params": {
    "percentage": 10
  }
}
```

Sample response:

```json
{
  "message": "discounts.rule.created",
  "rule": {
    "id": 5,
    "title": "order-total-higher-than-500-euro",
    "condition": "order-total-higher-than",
    "condition-params": {
      "amount": 500
    },
    "action": "add-order-item-percentage-discount",
    "action-params": {
      "percentage": 10
    }
  }
}
```

#### Modify an existing rule

`PUT http://localhost:8888/api/discounts/rules/{rule-id}`

Example request:

```json
{
  "title": "order-total-between-500-and-1000-euro",
  "condition": "order-total-between",
  "condition-params": {
    "amountFrom": 500,
    "amountUntil": 1000
  },
  "action": "add-order-item-percentage-discount",
  "action-params": {
    "percentage": 15
  }
}
```

Sample response:

```json
{
  "message": "discounts.rule.modified",
  "rule": {
    "id": 5,
    "title": "order-total-between-500-and-1000-euro",
    "condition": "order-total-between",
    "condition-params": {
      "amountFrom": 500,
      "amountUntil": 1000
    },
    "action": "add-order-item-percentage-discount",
    "action-params": {
      "percentage": 15
    }
  }
}
```

#### Remove an existing rule

`DELETE http://localhost:8888/api/discounts/rules/{rule-id}`

Sample response:

```json
{
  "message": "discounts.rule.deleted",
  "rule-id": 3
}
```

### Show available conditions

To get the conditions available for configuring rules, you can use:

`GET http://localhost:8888/api/discounts/conditions`

Sample response:

```json
[
  {
    "name": "customer-revenue-between",
    "class": "Jkeppens\\Discounts\\Domain\\Conditions\\Customer\\RevenueBetween",
    "params": {
      "amountFrom": {
        "required": false,
        "type": "float"
      },
      "amountUntil": {
        "required": false,
        "type": "float"
      }
    }
  },
  {
    "name": "customer-revenue-higher-than",
    "class": "Jkeppens\\Discounts\\Domain\\Conditions\\Customer\\RevenueHigherThan",
    "params": {
      "amount": {
        "required": true,
        "type": "float"
      }
    }
  },
  {
    "name": "customer-revenue-lower-than",
    "class": "Jkeppens\\Discounts\\Domain\\Conditions\\Customer\\RevenueLowerThan",
    "params": {
      "amount": {
        "required": true,
        "type": "float"
      }
    }
  },
  {
    "name": "customer-since-between",
    "class": "Jkeppens\\Discounts\\Domain\\Conditions\\Customer\\RevenueBetween",
    "params": {
      "dateFrom": {
        "required": false,
        "type": "date"
      },
      "dateUntil": {
        "required": false,
        "type": "date"
      }
    }
  },
  ...
]
```
### Show available actions 

To get the actions available for configuring rules, you can use:

`GET http://localhost:8888/api/discounts/actions`

Sample response:

```json
[
    {
        "name": "add-free-items",
        "class": "Jkeppens\\Discounts\\Domain\\Actions\\AddFreeItems",
        "params": {
            "productId": {
                "required": true,
                "type": "int"
            },
            "quantity": {
                "required": true,
                "type": "int"
            },
            "quantityPerChunkOf": {
                "required": false,
                "type": "int"
            }
        }
    },
    {
        "name": "add-order-item-percentage-discount",
        "class": "Jkeppens\\Discounts\\Domain\\Actions\\AddOrderItemPercentageDiscount",
        "params": {
            "percentage": {
                "required": true,
                "type": "float"
            }
        }
    },
    {
        "name": "add-order-item-value-discount",
        "class": "Jkeppens\\Discounts\\Domain\\Actions\\AddOrderItemValueDiscount",
        "params": {
            "discount": {
                "required": true,
                "type": "float"
            }
        }
    },
    ...
]
```

## Other Components

Apart from the `Discounts` component, there are 4 more components in the
project. These would be shared kernels for other micro services or standard
library code. All are found in the `library` directory and linked to in 
composer. These could be external composer components in a production environment.

### stdlib

This contains some shared classes / factories.

### catalog-shared

This contains logic to interact with and use the "products api". You can see
it as a "public interface" of some sorts. There would be another project `catalog`
that would contain catalog specific business logic and code.

Here it simple defines the `ProductRepository`, `ProductFileRepository` and entities
plus collections.

In a real world application there would be a `ProductRestRepository` here to connect
to the API.

### customer-shared

This is similar to catalog-shared but for everything related to customers.

As such it defines the `CustomerRepository`, `CustomerFileRepository` and entities
plus collections.

Likewise, in a real world application there would be a `CustomerRestRepository` here to connect
to the API.

### ordering-shared

This is similar to catalog-shared but for everything related to customers.

As such it defines the `OrderRepository`, `OrderFileRepository` and entities
plus collections.

Likewise, in a real world application there would be a `OrderRestRepository` here to connect
to the API.

## Testing

Several unit and feature tests have been written. Due to time constraints, I was not able to write all unit tests that I wanted to do.

The application layer needs unit tests for instance.

## Composer scripts available

### Built In Server

* **serve** - starts the webserver on port 8888

### Coding Style (PSR-12) checks

* **cs-check** - phpcs
* **cs-fix** - phpcbf

### Testing

* **test** - run unit tests
* **test-coverage** - run unit tests with coverage

### Mezzio / Laminas development mode

* **development-disable**
* **development-enable**
* **development-status**

### Configuration caching

By default, the skeleton will create a configuration cache in
`data/config-cache.php`. When in development mode, the configuration cache is
disabled, and switching in and out of development mode will remove the
configuration cache.

You may need to clear the configuration cache in production when deploying if
you deploy to the same directory. You may do so using the following:

```bash
$ composer clear-config-cache
```

## Known issues

- currently there are no checks on discounts higher than the amount of an item or order, this is obviously something that needs to be addressed.
- order discounts do not take into account product/order item discounts