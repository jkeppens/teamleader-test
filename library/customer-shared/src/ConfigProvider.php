<?php

namespace Jkeppens\Customer;

use Jkeppens\Customer\Domain\Repository\CustomerRepository;
use Jkeppens\Customer\Infrastructure\File\CustomerFileRepository;
use Jkeppens\Ordering\Infrastructure\File\OrderFileRepository;
use Psr\Container\ContainerInterface;
use RuntimeException;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'dependencies' => [
                'aliases' => [
                    CustomerRepository::class => CustomerFileRepository::class,
                ],
                'factories' => [
                    CustomerFileRepository::class => function (ContainerInterface $container)
                    {
                        /** @var array $config */
                        $config = $container->get('config');
                        if (empty($config['database']['json']['files']['customers'])) {
                            throw new RuntimeException('Customers data file path not configured');
                        }

                        return new CustomerFileRepository(
                            $config['database']['json']['files']['customers']
                        );
                    }
                ],
            ]
        ];
    }
}