<?php

namespace Jkeppens\Customer\Domain\Entity;

use Ramsey\Collection\Collection;

class CustomerEntityCollection extends Collection
{
    public function __construct(array $data = [])
    {
        parent::__construct(CustomerEntity::class, $data);
    }
}
