<?php

namespace Jkeppens\Customer\Domain\Entity;

use Carbon\Carbon;
use Jkeppens\Customer\Domain\Entity\Exception\CustomerMissingDataException;

class CustomerEntity
{
    /**
     * @param int $id
     * @param string $name
     * @param Carbon $since
     * @param int $revenueInCents
     */
    public function __construct(
        private readonly int $id,
        private readonly string $name,
        private readonly Carbon $since,
        private readonly int $revenueInCents
    ) {
    }

    /**
     * @param array $customer
     * @return static
     */
    public static function fromArray(array $customer): self
    {
        self::checkValidCustomerArray($customer);
        return new self(
            $customer['id'],
            $customer['name'],
            new Carbon($customer['since']),
            !empty($customer['revenue']) ? round($customer['revenue'] * 100) : 0
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'since' => $this->getSince()->format('Y-m-d'),
            'total' => $this->getRevenue(),
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Carbon
     */
    public function getSince(): Carbon
    {
        return $this->since;
    }

    /**
     * @return float
     */
    public function getRevenue(): float
    {
        return $this->getRevenueInCents() / 100;
    }

    /**
     * @return int
     */
    public function getRevenueInCents(): int
    {
        return $this->revenueInCents;
    }

    /**
     * This is basic validation, it does not check values for being a positive number for example
     *
     * @param array $customer
     * @return void
     */
    public static function checkValidCustomerArray(array $customer): void
    {
        $missingFields = [];
        if (is_null($customer['id'] ?? null)) {
            $missingFields[] = 'id';
        }
        if (is_null($customer['name'] ?? null)) {
            $missingFields[] = 'name';
        }
        if (is_null($customer['since'] ?? null)) {
            $missingFields[] = 'since';
        }
        if (!empty($missingFields)) {
            throw new CustomerMissingDataException($missingFields);
        }
    }
}
