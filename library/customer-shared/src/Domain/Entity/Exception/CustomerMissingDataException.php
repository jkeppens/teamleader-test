<?php

namespace Jkeppens\Customer\Domain\Entity\Exception;

use DomainException;

class CustomerMissingDataException extends DomainException
{
    /**
     * @param array $missingFields
     */
    public function __construct(private readonly array $missingFields)
    {
        parent::__construct('CUSTOMER.DOMAIN.ORDER.MISSING_FIELDS', 400);
    }

    /**
     * @return array
     */
    public function getMissingFields(): array
    {
        return $this->missingFields;
    }
}
