<?php

namespace Jkeppens\Customer\Domain\Repository;

use Jkeppens\Customer\Domain\Entity\CustomerEntity;
use Jkeppens\Customer\Domain\Entity\CustomerEntityCollection;

interface CustomerRepository
{
    /**
     * @param int $customerId
     * @return CustomerEntity|null
     */
    public function find(int $customerId): ?CustomerEntity;

    /**
     * @return CustomerEntityCollection
     */
    public function fetchAll(): CustomerEntityCollection;
}
