<?php

namespace Jkeppens\Customer\Infrastructure\File;

use Jkeppens\Customer\Domain\Entity\CustomerEntity;
use Jkeppens\Customer\Domain\Entity\CustomerEntityCollection;
use Jkeppens\Customer\Domain\Repository\CustomerRepository;
use Jkeppens\Customer\Infrastructure\File\Exception\CustomerDataFileNotFoundException;

class CustomerFileRepository implements CustomerRepository
{
    /** @var array */
    private array $cachedData = [];

    /**
     * @param string $dataFile
     */
    public function __construct(
        private readonly string $dataFile
    ) {
        if (!file_exists($this->dataFile)) {
            throw new CustomerDataFileNotFoundException();
        }
    }

    /**
     * @param int $customerId
     * @return CustomerEntity|null
     */
    public function find(int $customerId): ?CustomerEntity
    {
        $customers = array_map(
            fn ($customer) => CustomerEntity::fromArray($customer),
            array_filter(
                $this->readData(),
                fn ($record) => ($record['id'] == $customerId)
            )
        );

        if (empty($customers)) {
            return null;
        }

        return reset($customers);
    }

    /**
     * @return CustomerEntityCollection
     */
    public function fetchAll(): CustomerEntityCollection
    {
        return new CustomerEntityCollection(
            array_map(
                fn (array $customer) => CustomerEntity::fromArray($customer),
                $this->readData()
            )
        );
    }

    /**
     * @param bool $useCached
     * @return array
     */
    private function readData(bool $useCached = true): array
    {
        if (!$useCached || empty($this->cachedData)) {
            $this->cachedData = json_decode(file_get_contents($this->dataFile), true);
        }
        return $this->cachedData;
    }
}
