<?php

namespace Jkeppens\Customer\Infrastructure\File\Exception;

use RuntimeException;

class CustomerNotFoundException extends RuntimeException
{
    /**
     * @param int $customerId
     */
    public function __construct(private readonly int $customerId)
    {
        parent::__construct('CUSTOMER.INFRASTRUCTURE.CUSTOMER.NOT_FOUND', 404);
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }
}
