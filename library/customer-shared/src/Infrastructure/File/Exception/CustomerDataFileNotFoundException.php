<?php

namespace Jkeppens\Customer\Infrastructure\File\Exception;

use RuntimeException;

class CustomerDataFileNotFoundException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('CUSTOMER.INFRASTRUCTURE.CUSTOMER.DATA_FILE_NOT_FOUND', 500);
    }
}
