<?php

namespace Jkeppens\Catalog;

use Jkeppens\Catalog\Domain\Repository\ProductRepository;
use Jkeppens\Catalog\Infrastructure\File\ProductFileRepository;
use Psr\Container\ContainerInterface;
use RuntimeException;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'dependencies' => [
                'aliases' => [
                    ProductRepository::class => ProductFileRepository::class,
                ],
                'factories' => [
                    ProductFileRepository::class => function (ContainerInterface $container)
                    {
                        /** @var array $config */
                        $config = $container->get('config');
                        if (empty($config['database']['json']['files']['products'])) {
                            throw new RuntimeException('Products data file path not configured');
                        }

                        return new ProductFileRepository(
                            $config['database']['json']['files']['products']
                        );
                    }
                ],
            ]
        ];
    }
}