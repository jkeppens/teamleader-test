<?php

namespace Jkeppens\Catalog\Domain\Entity\Exception;

use DomainException;

class ProductMissingDataException extends DomainException
{
    /**
     * @param array $missingFields
     */
    public function __construct(private readonly array $missingFields)
    {
        parent::__construct('CATALOG.DOMAIN.PRODUCT.MISSING_FIELDS', 400);
    }

    /**
     * @return array
     */
    public function getMissingFields(): array
    {
        return $this->missingFields;
    }
}
