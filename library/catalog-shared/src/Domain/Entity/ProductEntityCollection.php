<?php

namespace Jkeppens\Catalog\Domain\Entity;

use Ramsey\Collection\Collection;

class ProductEntityCollection extends Collection
{
    public function __construct(array $data = [])
    {
        parent::__construct(ProductEntity::class, $data);
    }
}
