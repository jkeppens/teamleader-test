<?php

namespace Jkeppens\Catalog\Domain\Entity;

use Jkeppens\Catalog\Domain\Entity\Exception\ProductMissingDataException;

class ProductEntity
{
    /**
     * @param string $id
     * @param string $description
     * @param int $category
     * @param int $priceInCents
     */
    public function __construct(
        private readonly string $id,
        private readonly string $description,
        private readonly int $category,
        private readonly int $priceInCents
    ) {
    }

    /**
     * @param array $customer
     * @return static
     */
    public static function fromArray(array $customer): self
    {
        self::checkValidPriceArray($customer);
        return new self(
            $customer['id'],
            $customer['description'],
            $customer['category'],
            $customer['price'] * 100
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getDescription(),
            'since' => $this->getCategory(),
            'total' => $this->getPrice(),
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getCategory(): int
    {
        return $this->category;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->getPriceInCents() / 100;
    }

    /**
     * @return int
     */
    public function getPriceInCents(): int
    {
        return $this->priceInCents;
    }


    /**
     * This is basic validation, it does not check values for being a positive number for example
     *
     * @param array $customer
     * @return void
     */
    public static function checkValidPriceArray(array $customer): void
    {
        $missingFields = [];
        if (is_null($customer['id'] ?? null)) {
            $missingFields[] = 'id';
        }
        if (is_null($customer['description'] ?? null)) {
            $missingFields[] = 'description';
        }
        if (is_null($customer['category'] ?? null)) {
            $missingFields[] = 'category';
        }
        if (is_null($customer['price'] ?? null)) {
            $missingFields[] = 'price';
        }
        if (!empty($missingFields)) {
            throw new ProductMissingDataException($missingFields);
        }
    }
}