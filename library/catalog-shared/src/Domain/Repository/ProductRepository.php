<?php

namespace Jkeppens\Catalog\Domain\Repository;

use Jkeppens\Catalog\Domain\Entity\ProductEntity;
use Jkeppens\Catalog\Domain\Entity\ProductEntityCollection;

interface ProductRepository
{
    /**
     * @param string $productId
     * @return ProductEntity|null
     */
    public function find(string $productId): ?ProductEntity;

    /**
     * @return ProductEntityCollection
     */
    public function fetchAll(): ProductEntityCollection;
}
