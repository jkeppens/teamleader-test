<?php

namespace Jkeppens\Catalog\Infrastructure\File;

use Jkeppens\Catalog\Domain\Entity\ProductEntity;
use Jkeppens\Catalog\Domain\Entity\ProductEntityCollection;
use Jkeppens\Catalog\Domain\Repository\ProductRepository;
use Jkeppens\Catalog\Infrastructure\File\Exception\ProductDataFileNotFoundException;

class ProductFileRepository implements ProductRepository
{
    /** @var array */
    private array $cachedData = [];

    /**
     * @param string $dataFile
     */
    public function __construct(
        private readonly string $dataFile
    ) {
        if (!file_exists($this->dataFile)) {
            throw new ProductDataFileNotFoundException();
        }
    }

    /**
     * @param string $productId
     * @return ProductEntity|null
     */
    public function find(string $productId): ?ProductEntity
    {
        $products = array_map(
            fn ($customer) => ProductEntity::fromArray($customer),
            array_filter(
                $this->readData(),
                fn ($record) => ($record['id'] == $productId)
            )
        );

        if (empty($products)) {
            return null;
        }

        return reset($products);
    }

    /**
     * @return ProductEntityCollection
     */
    public function fetchAll(): ProductEntityCollection
    {
        return new ProductEntityCollection(
            array_map(
                fn (array $customer) => ProductEntity::fromArray($customer),
                $this->readData()
            )
        );
    }

    /**
     * @param bool $useCached
     * @return array
     */
    private function readData(bool $useCached = true): array
    {
        if (!$useCached || empty($this->cachedData)) {
            $this->cachedData = json_decode(file_get_contents($this->dataFile), true);
        }
        return $this->cachedData;
    }
}
