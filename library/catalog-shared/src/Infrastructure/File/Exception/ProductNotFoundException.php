<?php

namespace Jkeppens\Catalog\Infrastructure\File\Exception;

use RuntimeException;

class ProductNotFoundException extends RuntimeException
{
    /**
     * @param int $productId
     */
    public function __construct(private readonly int $productId)
    {
        parent::__construct('CATALOG.INFRASTRUCTURE.PRODUCT.NOT_FOUND', 404);
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }
}
