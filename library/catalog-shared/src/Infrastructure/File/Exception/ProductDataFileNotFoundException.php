<?php

namespace Jkeppens\Catalog\Infrastructure\File\Exception;

use RuntimeException;

class ProductDataFileNotFoundException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('CATALOG.INFRASTRUCTURE.PRODUCT.DATA_FILE_NOT_FOUND', 500);
    }
}
