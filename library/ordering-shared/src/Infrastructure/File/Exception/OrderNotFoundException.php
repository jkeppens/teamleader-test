<?php

namespace Jkeppens\Ordering\Infrastructure\File\Exception;

use RuntimeException;

class OrderNotFoundException extends RuntimeException
{
    /**
     * @param int $orderId
     */
    public function __construct(private readonly int $orderId)
    {
        parent::__construct('ORDERING.INFRASTRUCTURE.ORDER.NOT_FOUND', 404);
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }
}
