<?php

namespace Jkeppens\Ordering\Infrastructure\File;

use Jkeppens\Catalog\Infrastructure\File\ProductFileRepository;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregate;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregateFactory;
use Jkeppens\Ordering\Domain\Repository\OrderRepository;
use Jkeppens\Ordering\Infrastructure\File\Exception\OrderNotFoundException;

class OrderFileRepository implements OrderRepository
{
    /**
     * @param string $fileRoot
     * @param OrderAggregateFactory $orderAggregateFactory
     */
    public function __construct(
        private readonly string $fileRoot,
        private readonly OrderAggregateFactory $orderAggregateFactory
    ) {
    }

    /**
     * @param int $orderId
     * @return OrderAggregate|null
     */
    public function find(int $orderId): ?OrderAggregate
    {
        $orderData = $this->readOrderFile($orderId);
        return $this->orderAggregateFactory->fromArray($orderData);
    }

    /**
     * @param int $orderId
     * @return array
     */
    private function readOrderFile(int $orderId): array
    {
        $file = sprintf(
            '%s/order%d.json',
            rtrim($this->fileRoot, DIRECTORY_SEPARATOR),
            $orderId
        );
        if (!file_exists($file)) {
            throw new OrderNotFoundException($orderId);
        }
        return json_decode(file_get_contents($file), true);
    }
}
