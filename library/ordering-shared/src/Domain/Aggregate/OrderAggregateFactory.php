<?php

namespace Jkeppens\Ordering\Domain\Aggregate;

use Jkeppens\Catalog\Infrastructure\File\ProductFileRepository;

class OrderAggregateFactory
{
    public function __construct(
        private readonly ProductFileRepository $productFileRepository
    ) {
    }

    /**
     * @param array $orderData
     * @return OrderAggregate
     */
    public function fromArray(array $orderData): OrderAggregate
    {
        foreach ($orderData['items'] as $key => $item) {
            $orderData['items'][$key]['product'] = $this->productFileRepository->find($item['product-id']);
        }
        return OrderAggregate::fromArray($orderData);
    }
}
