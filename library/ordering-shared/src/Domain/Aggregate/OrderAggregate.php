<?php

namespace Jkeppens\Ordering\Domain\Aggregate;

use Jkeppens\Ordering\Domain\Entity\OrderEntity;
use Jkeppens\Ordering\Domain\Entity\OrderItemEntity;
use Jkeppens\Ordering\Domain\Entity\OrderItemEntityCollection;

class OrderAggregate
{
    /**
     * @param OrderEntity $order
     * @param OrderItemAggregateCollection $items
     */
    public function __construct(
        private OrderEntity $order,
        private OrderItemAggregateCollection $items
    ) {
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        $order = OrderEntity::fromArray($data);
        $items = new OrderItemAggregateCollection(
            array_map(
                fn (array $item) => OrderItemAggregate::fromArray($item),
                $data['items'] ?? []
            )
        );
        return new self($order, $items);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = $this->getOrder()->toArray();
        $data['items'] = $this->getItems()->toArray();
        return $data;
    }

    /**
     * @return OrderEntity
     */
    public function getOrder(): OrderEntity
    {
        return $this->order;
    }

    /**
     * @return OrderItemAggregateCollection
     */
    public function getItems(): OrderItemAggregateCollection
    {
        return $this->items;
    }
}
