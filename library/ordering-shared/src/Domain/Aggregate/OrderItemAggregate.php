<?php

namespace Jkeppens\Ordering\Domain\Aggregate;

use Jkeppens\Catalog\Domain\Entity\ProductEntity;
use Jkeppens\Ordering\Domain\Entity\OrderItemEntity;

class OrderItemAggregate
{
    /**
     * @param OrderItemEntity $orderItem
     * @param ProductEntity $product
     */
    public function __construct(
        private readonly OrderItemEntity $orderItem,
        private readonly ProductEntity $product
    ) {
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        $order = OrderItemEntity::fromArray($data);
        $product = is_array($data['product']) ? ProductEntity::fromArray($data['product'] ?? []) : $data['product'];
        return new self($order, $product);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = $this->getOrderItem()->toArray();
        $data['product'] = $this->getProduct()->toArray();
        return $data;
    }

    /**
     * @return OrderItemEntity
     */
    public function getOrderItem(): OrderItemEntity
    {
        return $this->orderItem;
    }

    /**
     * @return ProductEntity
     */
    public function getProduct(): ProductEntity
    {
        return $this->product;
    }
}
