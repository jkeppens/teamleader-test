<?php

namespace Jkeppens\Ordering\Domain\Aggregate;

use Ramsey\Collection\Collection;

class OrderItemAggregateCollection extends Collection
{
    public function __construct(array $data = [])
    {
        parent::__construct(
            OrderItemAggregate::class,
            array_map(
                fn ($orderItem) => is_array($orderItem) ? OrderItemAggregate::fromArray($orderItem) : $orderItem,
                $data
            )
        );
    }
}
