<?php

namespace Jkeppens\Ordering\Domain\Repository;

use Jkeppens\Ordering\Domain\Aggregate\OrderAggregate;

interface OrderRepository
{
    public function find(int $orderId): ?OrderAggregate;
}
