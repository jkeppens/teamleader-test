<?php

namespace Jkeppens\Ordering\Domain\Entity;

use Jkeppens\Ordering\Domain\Entity\Exception\OrderItemMissingDataException;

class OrderItemEntity
{
    /**
     * @param string $productId
     * @param int $quantity
     * @param int $unitPriceInCents
     * @param int|null $totalInCents
     */
    public function __construct(
        private readonly string $productId,
        private int $quantity,
        private int $unitPriceInCents,
        private int|null $totalInCents = null
    )
    {
        if (is_null($this->totalInCents)) {
            $this->totalInCents = $this->quantity * $this->unitPriceInCents;
        }
    }

    /**
     * @param array $orderItem
     * @return static
     */
    public static function fromArray(array $orderItem): self
    {
        self::checkValidOrderItemArray($orderItem);
        return new self(
            $orderItem['product-id'],
            $orderItem['quantity'],
            round($orderItem['unit-price'] * 100),
            !empty($orderItem['total']) ? round($orderItem['total'] * 100) : null
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'product-id' => $this->getProductId(),
            'quantity' => $this->getQuantity(),
            'unit-price' => $this->getUnitPrice(),
            'total' => $this->getTotal(),
        ];
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getUnitPrice(): float
    {
        return $this->unitPriceInCents / 100;
    }

    /**
     * @return int
     */
    public function getUnitPriceInCents(): int
    {
        return $this->unitPriceInCents;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->totalInCents / 100;
    }

    /**
     * @return int
     */
    public function getTotalInCents(): int
    {
        return $this->totalInCents;
    }


    /**
     * This is basic validation, it does not check values for being a positive number for example
     *
     * @param array $orderItem
     * @return void
     */
    public static function checkValidOrderItemArray(array $orderItem): void
    {
        $missingFields = [];
        if (is_null($orderItem['product-id'] ?? null)) {
            $missingFields[] = 'product-id';
        }
        if (is_null($orderItem['quantity'] ?? null)) {
            $missingFields[] = 'quantity';
        }
        if (is_null($orderItem['unit-price'] ?? null)) {
            $missingFields[] = 'unit-price';
        }
        if (!empty($missingFields)) {
            throw new OrderItemMissingDataException($missingFields);
        }
    }
}
