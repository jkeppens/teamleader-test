<?php

namespace Jkeppens\Ordering\Domain\Entity;

use Ramsey\Collection\Collection;

class OrderEntityCollection extends Collection
{
    public function __construct(array $data = [])
    {
        parent::__construct(OrderEntity::class, $data);
    }
}
