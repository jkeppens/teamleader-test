<?php

namespace Jkeppens\Ordering\Domain\Entity;

use Ramsey\Collection\Collection;

class OrderItemEntityCollection extends Collection
{
    public function __construct(array $data = [])
    {
        parent::__construct(OrderItemEntity::class, $data);
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->getTotalInCents() / 100;
    }

    /**
     * @return int
     */
    public function getTotalInCents(): int
    {
        $total = 0;

        /** @var OrderItemEntity $item */
        foreach ($this->data as $item) {
            $total+= $item->getTotalInCents();
        }

        return $total;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_map(
            fn (OrderItemEntity $item) => $item->toArray(),
            parent::toArray()
        );
    }
}
