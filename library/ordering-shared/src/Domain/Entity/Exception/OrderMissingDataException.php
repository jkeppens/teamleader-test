<?php

namespace Jkeppens\Ordering\Domain\Entity\Exception;

use DomainException;

class OrderMissingDataException extends DomainException
{
    /**
     * @param array $missingFields
     */
    public function __construct(private readonly array $missingFields)
    {
        parent::__construct('ORDERING.DOMAIN.ORDER.MISSING_FIELDS', 400);
    }

    /**
     * @return array
     */
    public function getMissingFields(): array
    {
        return $this->missingFields;
    }
}
