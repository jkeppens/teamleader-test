<?php

namespace Jkeppens\Ordering\Domain\Entity;

use Jkeppens\Ordering\Domain\Entity\Exception\OrderMissingDataException;

class OrderEntity
{
    /**
     * @param string $id
     * @param int $customerId
     * @param int $totalInCents
     */
    public function __construct(
        private readonly string $id,
        private readonly int $customerId,
        private readonly int $totalInCents
    ) {
    }

    /**
     * @param array $orderItem
     * @return static
     */
    public static function fromArray(array $orderItem): self
    {
        self::checkValidOrderArray($orderItem);
        return new self(
            $orderItem['id'],
            $orderItem['customer-id'],
            !empty($orderItem['total']) ? round($orderItem['total'] * 100) : null
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'customer-id' => $this->getCustomerId(),
            'total' => $this->getTotal(),
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->totalInCents / 100;
    }

    /**
     * @return int
     */
    public function getTotalInCents(): int
    {
        return $this->totalInCents;
    }


    /**
     * This is basic validation, it does not check values for being a positive number for example
     *
     * @param array $order
     * @return void
     */
    public static function checkValidOrderArray(array $order): void
    {
        $missingFields = [];
        if (is_null($order['id'] ?? null)) {
            $missingFields[] = 'id';
        }
        if (is_null($order['customer-id'] ?? null)) {
            $missingFields[] = 'customer-id';
        }
        if (!empty($missingFields)) {
            throw new OrderMissingDataException($missingFields);
        }
    }
}
