<?php

namespace Jkeppens\Ordering;

use Jkeppens\Catalog\Domain\Repository\ProductRepository;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregateFactory;
use Jkeppens\Ordering\Domain\Repository\OrderRepository;
use Jkeppens\Ordering\Infrastructure\File\OrderFileRepository;
use Psr\Container\ContainerInterface;
use RuntimeException;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'dependencies' => [
                'aliases' => [
                    OrderRepository::class => OrderFileRepository::class,
                ],
                'factories' => [
                    OrderFileRepository::class => function (ContainerInterface $container)
                    {
                        /** @var array $config */
                        $config = $container->get('config');
                        if (empty($config['database']['json']['paths']['orders'])) {
                            throw new RuntimeException('Order files path not configured');
                        }

                        $orderAggregateFactory = $container->get(OrderAggregateFactory::class);

                        return new OrderFileRepository(
                            $config['database']['json']['paths']['orders'],
                            $orderAggregateFactory
                        );
                    },
                    OrderAggregateFactory::class => function (ContainerInterface $container)
                    {
                        $productRepository = $container->get(ProductRepository::class);
                        return new OrderAggregateFactory(
                            $productRepository
                        );
                    },
                ],
            ]
        ];
    }
}