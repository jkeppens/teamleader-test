<?php

namespace Jkeppens\Stdlib\UI\Http\Handler;

use Laminas\ServiceManager\Factory\FactoryInterface;
use League\Tactician\CommandBus;
use Psr\Container\ContainerInterface;

class CommandBusServerRequestHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        return new $requestedName(
            $container->get(CommandBus::class)
        );
    }
}