<?php

declare(strict_types=1);

namespace Jkeppens\Discounts;

use Jkeppens\Customer\Domain\Repository\CustomerRepository;
use Jkeppens\Discounts\Application\Command\Rule\CreateRuleCommandHandler;
use Jkeppens\Discounts\Application\Command\Rule\DeleteRuleCommandHandler;
use Jkeppens\Discounts\Application\Command\Rule\ModifyRuleCommandHandler;
use Jkeppens\Discounts\Application\Command\Rule\RuleConditionActionRepositoryAndEventDispatcherCommandHandlerFactory;
use Jkeppens\Discounts\Application\Command\Rule\RuleRepositoryAndEventDispatcherCommandHandlerFactory;
use Jkeppens\Discounts\Application\Service\CalculateDiscountService;
use Jkeppens\Discounts\Domain\Aggregate\DiscountAggregate;
use Jkeppens\Discounts\Domain\Enum\DiscountClashResolvementEnum;
use Jkeppens\Discounts\Domain\Repository\ActionRepository;
use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use Jkeppens\Discounts\Domain\Repository\ConditionRepository;
use Jkeppens\Discounts\Infrastructure\File\Json\RuleFileRepository;
use Jkeppens\Discounts\Infrastructure\File\Php\ActionFileRepository;
use Jkeppens\Discounts\Infrastructure\File\Php\ConditionFileRepository;
use Jkeppens\Discounts\UI\Http\Handler\CalculateDiscountForOrderHandler;
use Jkeppens\Discounts\UI\Http\Handler\CalculateDiscountForOrderIdHandler;
use Jkeppens\Discounts\UI\Http\Handler\Action\ListActionsHandler;
use Jkeppens\Discounts\UI\Http\Handler\Condition\ListConditionsHandler;
use Jkeppens\Discounts\UI\Http\Handler\Rule\CreateRuleHandler;
use Jkeppens\Discounts\UI\Http\Handler\Rule\DeleteRuleHandler;
use Jkeppens\Discounts\UI\Http\Handler\Rule\ListRulesHandler;
use Jkeppens\Discounts\UI\Http\Handler\Rule\ModifyRuleHandler;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregateFactory;
use Jkeppens\Ordering\Domain\Repository\OrderRepository;
use Jkeppens\Stdlib\UI\Http\Handler\CommandBusServerRequestHandlerFactory;
use Psr\Container\ContainerInterface;
use RuntimeException;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.laminas.dev/laminas-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                'aliases' => [
                ],
                'factories' => [
                    CreateRuleHandler::class => CommandBusServerRequestHandlerFactory::class,
                    CreateRuleCommandHandler::class => RuleConditionActionRepositoryAndEventDispatcherCommandHandlerFactory::class,

                    ModifyRuleHandler::class => CommandBusServerRequestHandlerFactory::class,
                    ModifyRuleCommandHandler::class => RuleConditionActionRepositoryAndEventDispatcherCommandHandlerFactory::class,

                    DeleteRuleHandler::class => CommandBusServerRequestHandlerFactory::class,
                    DeleteRuleCommandHandler::class => RuleRepositoryAndEventDispatcherCommandHandlerFactory::class,

                    RuleFileRepository::class => function (ContainerInterface $container) {
                        /** @var array $config */
                        $config = $container->get('config');
                        if (empty($config['database']['json']['files']['rules'])) {
                            throw new RuntimeException('Rules files path not configured');
                        }

                        return new RuleFileRepository(
                            $config['database']['json']['files']['rules']
                        );
                    },
                    ConditionFileRepository::class => function (ContainerInterface $container) {
                        /** @var array $config */
                        $config = $container->get('config');
                        if (empty($config['database']['php']['files']['conditions'])) {
                            throw new RuntimeException('Conditions files path not configured');
                        }

                        return new ConditionFileRepository(
                            $config['database']['php']['files']['conditions']
                        );
                    },
                    ActionFileRepository::class => function (ContainerInterface $container) {
                        /** @var array $config */
                        $config = $container->get('config');
                        if (empty($config['database']['php']['files']['actions'])) {
                            throw new RuntimeException('Action files path not configured');
                        }

                        return new ActionFileRepository(
                            $config['database']['php']['files']['actions']
                        );
                    },
                    CalculateDiscountService::class => function (ContainerInterface $container) {
                        return new CalculateDiscountService(
                            $container->get(RuleRepository::class),
                            $container->get(CustomerRepository::class),
                            $container->get(ConditionRepository::class),
                            $container->get(ActionRepository::class),
                            $container->get(DiscountAggregate::class)
                        );
                    },
                    DiscountAggregate::class => function (ContainerInterface $container) {
                        $config = $container->get('config');
                        $useHighestValue = DiscountClashResolvementEnum::UseHighestValue;
                        $addDiscount = DiscountClashResolvementEnum::AddDiscount;
                        return new DiscountAggregate(
                            $config['discount-strategy']['order'] ?? $useHighestValue,
                            $config['discount-strategy']['order-items'] ?? $useHighestValue,
                            $config['discount-strategy']['free-items'] ?? $addDiscount
                        );
                    },
                    CalculateDiscountForOrderHandler::class => function (ContainerInterface $container) {
                        return new CalculateDiscountForOrderHandler(
                            $container->get(CalculateDiscountService::class),
                            $container->get(OrderAggregateFactory::class)
                        );
                    },
                    CalculateDiscountForOrderIdHandler::class => function (ContainerInterface $container) {
                        return new CalculateDiscountForOrderIdHandler(
                            $container->get(OrderRepository::class),
                            $container->get(CalculateDiscountService::class)
                        );
                    },
                    ListRulesHandler::class => function (ContainerInterface $container) {
                        return new ListRulesHandler(
                            $container->get(RuleRepository::class)
                        );
                    },
                    ListActionsHandler::class => function (ContainerInterface $container) {
                        return new ListActionsHandler(
                            $container->get(ActionRepository::class)
                        );
                    },
                    ListConditionsHandler::class => function (ContainerInterface $container) {
                        return new ListConditionsHandler(
                            $container->get(ConditionRepository::class)
                        );
                    }
                ],
            ],
        ];
    }
}
