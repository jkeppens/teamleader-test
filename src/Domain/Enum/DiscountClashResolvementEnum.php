<?php

namespace Jkeppens\Discounts\Domain\Enum;

enum DiscountClashResolvementEnum
{
    case UseHighestValue;
    case UseLowestValue;
    case UseFirstDiscount;
    case UseLastDiscount;
    case AddDiscount;
}
