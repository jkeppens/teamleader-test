<?php

namespace Jkeppens\Discounts\Domain\Aggregate;

use Jkeppens\Discounts\Domain\Actions\ActionInterface;
use Jkeppens\Discounts\Domain\Conditions\ConditionInterface;
use Jkeppens\Discounts\Domain\Entity\ActionEntity;
use Jkeppens\Discounts\Domain\Entity\Discount\FreeItemEntity;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderDiscountEntity;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntity;
use Jkeppens\Discounts\Domain\Entity\ConditionEntity;

class RulesAggregate
{
    /**
     * @param RuleEntity $rule
     * @param ConditionEntity $conditionEntity
     * @param ActionEntity $actionEntity
     * @param ConditionInterface $condition
     * @param ActionInterface|null $action
     */
    public function __construct(
        private readonly RuleEntity $rule,
        private readonly ConditionEntity $conditionEntity,
        private readonly ActionEntity $actionEntity,
        private readonly ConditionInterface $condition,
        private readonly ActionInterface|null $action
    ) {
    }

    /**
     * @return RuleEntity
     */
    public function getRule(): RuleEntity
    {
        return $this->rule;
    }

    /**
     * @return ConditionEntity
     */
    public function getConditionEntity(): ConditionEntity
    {
        return $this->conditionEntity;
    }

    /**
     * @return ActionEntity
     */
    public function getActionEntity(): ActionEntity
    {
        return $this->actionEntity;
    }

    /**
     * @return ConditionInterface
     */
    public function getCondition(): ConditionInterface
    {
        return $this->condition;
    }

    /**
     * @return ActionInterface
     */
    public function getAction(): ActionInterface
    {
        return $this->action;
    }

    /**
     * @param array $rule
     * @return static
     */
    public static function fromArray(array $rule): self
    {
        return new RulesAggregate(
            $rule['rule'] ?? null,
            $rule['condition-entity'] ?? null,
            $rule['action-entity'] ?? null,
            $rule['condition'] ?? null,
            $rule['action'] ?? null,
        );
    }

    /**
     * @param DiscountAggregate $discounts
     * @param OrderWithContextAggregate $order
     * @return void
     */
    public function calculateDiscount(
        DiscountAggregate $discounts,
        OrderWithContextAggregate $order
    ): void {
        $appliesTo = $this->getCondition()->appliesTo($order);
        if (empty($appliesTo)) {
            return;
        }
        if (!is_array($appliesTo)) {
            $appliesTo = [$appliesTo];
        }

        $action = $this->getAction();
        foreach ($appliesTo as $appliesToItem) {
            $discount = $action->apply($appliesToItem);
            if (is_null($discount)) {
                continue;
            }
            $discount->setReason($this->rule->getTitle());
            if ($discount instanceof OrderDiscountEntity) {
                $discounts->addOrderDiscount($discount);
            } elseif ($discount instanceof OrderItemDiscountEntity) {
                $discounts->addOrderItemDiscount($discount);
            } elseif ($discount instanceof FreeItemEntity) {
                $discounts->addFreeItem($discount);
            }
        }
    }
}
