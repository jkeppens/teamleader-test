<?php

namespace Jkeppens\Discounts\Domain\Aggregate;

use Ramsey\Collection\Collection;

class RulesAggregateCollection extends Collection
{
    /**
     * @param array $data
     */
    public function __construct(
        private readonly DiscountAggregate $discounts
    ) {
        parent::__construct(RulesAggregate::class);
    }

    /**
     * @param OrderWithContextAggregate $order
     * @return void
     */
    public function calculateDiscounts(OrderWithContextAggregate $order): void
    {
        /** @var RulesAggregate $rulesAggregate */
        foreach ($this->data as $rulesAggregate) {
            $rulesAggregate->calculateDiscount($this->discounts, $order);
        }
    }

    /**
     * @return DiscountAggregate
     */
    public function getDiscounts(): DiscountAggregate
    {
        return $this->discounts;
    }
}
