<?php

namespace Jkeppens\Discounts\Domain\Aggregate;

use Jkeppens\Customer\Domain\Entity\CustomerEntity;
use Jkeppens\Discounts\Domain\Entity\Discount\FreeItemEntity;
use Jkeppens\Discounts\Domain\Entity\Discount\FreeItemEntityCollection;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderDiscountEntity;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntity;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntityCollection;
use Jkeppens\Discounts\Domain\Enum\DiscountClashResolvementEnum;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregate;

class DiscountAggregate
{
    private OrderDiscountEntity|null $orderDiscount = null;

    private FreeItemEntityCollection $freeItems;

    private OrderItemDiscountEntityCollection $orderItemDiscounts;

    /**
     *
     */
    public function __construct(
        private readonly DiscountClashResolvementEnum $orderDiscountStrategy,
        DiscountClashResolvementEnum $orderItemDiscountStrategy,
        DiscountClashResolvementEnum $freeItemsStrategy,
    ) {
        $this->freeItems = new FreeItemEntityCollection($freeItemsStrategy);
        $this->orderItemDiscounts = new OrderItemDiscountEntityCollection($orderItemDiscountStrategy);
    }

    /**
     * @param OrderDiscountEntity $orderDiscount
     * @return void
     */
    public function addOrderDiscount(OrderDiscountEntity $orderDiscount): void
    {
        if (empty($this->orderDiscount)) {
            $this->orderDiscount = $orderDiscount;
            return;
        }
        switch ($this->orderDiscountStrategy) {
            case DiscountClashResolvementEnum::AddDiscount:
                $discount = $this->orderDiscount->getDiscount() + $orderDiscount->getDiscount();
                $percentage = null;
                break;
            case DiscountClashResolvementEnum::UseFirstDiscount:
                $discount = $this->orderDiscount->getDiscount();
                $percentage = $this->orderDiscount->getPercentage();
                break;
            case DiscountClashResolvementEnum::UseLastDiscount:
                $discount = $orderDiscount->getDiscount();
                $percentage = $orderDiscount->getPercentage();
                break;
            case DiscountClashResolvementEnum::UseHighestValue:
                if ($this->orderDiscount->getDiscount() < $orderDiscount->getDiscount()) {
                    $discount = $orderDiscount->getDiscount();
                    $percentage = $orderDiscount->getPercentage();
                } else {
                    $discount = $this->orderDiscount->getDiscount();
                    $percentage = $this->orderDiscount->getPercentage();
                }
                break;
            case DiscountClashResolvementEnum::UseLowestValue:
                if ($this->orderDiscount->getDiscount() > $orderDiscount->getDiscount()) {
                    $discount = $orderDiscount->getDiscount();
                    $percentage = $orderDiscount->getPercentage();
                } else {
                    $discount = $this->orderDiscount->getDiscount();
                    $percentage = $this->orderDiscount->getPercentage();
                }
                break;
        }
        $this->orderDiscount = new OrderDiscountEntity(
            $discount,
            $percentage
        );
    }

    /**
     * @param FreeItemEntity $freeItemEntity
     * @return void
     */
    public function addFreeItem(FreeItemEntity $freeItemEntity): void
    {
        $this->freeItems->add($freeItemEntity);
    }

    /**
     * @param OrderItemDiscountEntity $orderItemDiscount
     * @return void
     */
    public function addOrderItemDiscount(OrderItemDiscountEntity $orderItemDiscount): void
    {
        $this->orderItemDiscounts->add($orderItemDiscount);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $discounts = [];
        if (!empty($this->orderDiscount)) {
            $discounts['order'] = $this->orderDiscount->toArray();
        }
        if ($this->orderItemDiscounts->count()) {
            $discounts['order-items'] = $this->orderItemDiscounts->toArray();
        }
        if ($this->freeItems->count()) {
            $discounts['free-items'] = $this->freeItems->toArray();
        }
        return $discounts;
    }

    /**
     * @return OrderDiscountEntity|null
     */
    public function getOrderDiscount(): ?OrderDiscountEntity
    {
        return $this->orderDiscount;
    }

    /**
     * @return OrderItemDiscountEntityCollection
     */
    public function getOrderItemDiscounts(): OrderItemDiscountEntityCollection
    {
        return $this->orderItemDiscounts;
    }

    /**
     * @return FreeItemEntityCollection
     */
    public function getFreeItems(): FreeItemEntityCollection
    {
        return $this->freeItems;
    }
}
