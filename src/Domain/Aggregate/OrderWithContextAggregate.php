<?php

namespace Jkeppens\Discounts\Domain\Aggregate;

use Jkeppens\Customer\Domain\Entity\CustomerEntity;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregate;

class OrderWithContextAggregate
{
    /**
     * @param OrderAggregate $order
     * @param CustomerEntity $customer
     */
    public function __construct(
        private readonly OrderAggregate $order,
        private readonly CustomerEntity $customer
    ) {
    }

    /**
     * @return OrderAggregate
     */
    public function getOrder(): OrderAggregate
    {
        return $this->order;
    }

    /**
     * @return CustomerEntity
     */
    public function getCustomer(): CustomerEntity
    {
        return $this->customer;
    }
}
