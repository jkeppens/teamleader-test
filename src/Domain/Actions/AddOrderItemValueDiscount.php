<?php

namespace Jkeppens\Discounts\Domain\Actions;

use Jkeppens\Discounts\Domain\Entity\Discount\DiscountInterface;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntity;
use Jkeppens\Ordering\Domain\Aggregate\OrderItemAggregate;
use Jkeppens\Ordering\Domain\Entity\OrderItemEntity;
use RuntimeException;

class AddOrderItemValueDiscount implements ActionInterface
{
    private readonly float $discount;

    /**
     * @param array $params
     */
    public function __construct(
        array $params
    ) {
        $this->discount = $params['discount'] ?? null;
    }

    /**
     * @param mixed $orderItem
     * @return DiscountInterface|null
     */
    public function apply(mixed $orderItem): DiscountInterface|null
    {
        if ($orderItem instanceof OrderItemAggregate) {
            $orderItem = $orderItem->getOrderItem();
        }
        if (!$orderItem instanceof OrderItemEntity) {
            throw new RuntimeException('Expects an order item');
        }

        if (!$this->discount) {
            return null;
        }

        return new OrderItemDiscountEntity(
            $orderItem->getProductId(),
            $this->discount,
        );
    }
}
