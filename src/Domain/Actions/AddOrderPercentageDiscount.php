<?php

namespace Jkeppens\Discounts\Domain\Actions;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Entity\Discount\DiscountInterface;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderDiscountEntity;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregate;
use Jkeppens\Ordering\Domain\Entity\OrderEntity;
use RuntimeException;

class AddOrderPercentageDiscount implements ActionInterface
{
    private readonly float $percentage;

    /**
     * @param array $params
     */
    public function __construct(
        array $params
    ) {
        $this->percentage = $params['percentage'] ?? null;
    }

    /**
     * @param OrderWithContextAggregate|OrderAggregate|OrderEntity $order
     * @return DiscountInterface|null
     */
    public function apply(mixed $order): DiscountInterface|null
    {
        if ($order instanceof OrderWithContextAggregate) {
            $order = $order->getOrder();
        }
        if ($order instanceof OrderAggregate) {
            $order = $order->getOrder();
        }
        if (!$order instanceof OrderEntity) {
            throw new RuntimeException('Expects an order');
        }

        $discount = round($order->getTotalInCents() * ($this->percentage / 100)) / 100;
        if (!$discount) {
            return null;
        }

        return new OrderDiscountEntity(
            $discount,
            $this->percentage
        );
    }
}
