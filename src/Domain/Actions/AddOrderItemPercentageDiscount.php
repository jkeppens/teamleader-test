<?php

namespace Jkeppens\Discounts\Domain\Actions;

use Jkeppens\Discounts\Domain\Entity\Discount\DiscountInterface;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntity;
use Jkeppens\Ordering\Domain\Aggregate\OrderItemAggregate;
use Jkeppens\Ordering\Domain\Entity\OrderItemEntity;
use RuntimeException;

class AddOrderItemPercentageDiscount implements ActionInterface
{
    private readonly float $percentage;

    /**
     * @param array $params
     */
    public function __construct(
        array $params
    ) {
        $this->percentage = $params['percentage'] ?? null;
    }

    /**
     * @param mixed $orderItem
     * @return DiscountInterface|null
     */
    public function apply(mixed $orderItem): DiscountInterface|null
    {
        if ($orderItem instanceof OrderItemAggregate) {
            $orderItem = $orderItem->getOrderItem();
        }
        if (!$orderItem instanceof OrderItemEntity) {
            throw new RuntimeException('Expects an order item');
        }

        $discount = round($orderItem->getTotalInCents() * ($this->percentage / 100)) / 100;
        if (!$discount) {
            return null;
        }

        return new OrderItemDiscountEntity(
            $orderItem->getProductId(),
            $discount,
            $this->percentage
        );
    }
}
