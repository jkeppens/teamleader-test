<?php

namespace Jkeppens\Discounts\Domain\Actions;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Entity\Discount\DiscountInterface;
use Jkeppens\Discounts\Domain\Entity\Discount\FreeItemEntity;
use Jkeppens\Ordering\Domain\Aggregate\OrderItemAggregate;
use Jkeppens\Ordering\Domain\Entity\OrderItemEntity;
use RuntimeException;

class AddFreeItems implements ActionInterface
{
    private readonly int $quantity;

    private readonly int|null $quantityPerChunkOf;

    private readonly string|null $productId;

    /**
     * @param array $params
     */
    public function __construct(
        array $params
    ) {
        $this->quantity = $params['quantity'] ?? 1;
        $this->quantityPerChunkOf = $params['quantityPerChunkOf'] ?? $params['quantity-per-chunk-of'] ?? null;
        $this->productId = $params['productId'] ?? $params['product-id'] ?? null;
    }

    /**
     * @param mixed $orderItem
     * @return DiscountInterface|null
     */
    public function apply(mixed $orderItem): DiscountInterface|null
    {
        if ($orderItem instanceof OrderWithContextAggregate) {
            if (empty($this->productId)) {
                throw new RuntimeException('Product ID required when passing an order');
            }
            if (!empty($this->quantityPerChunkOf)) {
                throw new RuntimeException('QuantityPerChunkOf not possible when passing an order');
            }
            $productId = $this->productId;
            $multiplier = 1;
        } else {
            if ($orderItem instanceof OrderItemAggregate) {
                $orderItem = $orderItem->getOrderItem();
            }
            if (!$orderItem instanceof OrderItemEntity) {
                throw new RuntimeException('Expects an order item');
            }
            $productId = !empty($this->productId) ? $this->productId : $orderItem->getProductId();
            $multiplier = empty($this->quantityPerChunkOf) ? 1 : floor($orderItem->getQuantity() / $this->quantityPerChunkOf);
        }

        if (!$multiplier) {
            return null;
        }
        $quantity = $this->quantity * $multiplier;

        return new FreeItemEntity(
            $productId,
            $quantity
        );
    }
}
