<?php

namespace Jkeppens\Discounts\Domain\Actions;

use Jkeppens\Discounts\Domain\Entity\Discount\DiscountInterface;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntity;
use Jkeppens\Ordering\Domain\Aggregate\OrderItemAggregate;
use Jkeppens\Ordering\Domain\Aggregate\OrderItemAggregateCollection;
use LogicException;
use RuntimeException;

class AddOrderItemPercentageDiscountOnOneOfTheItems implements ActionInterface
{
    private readonly string $checkOn;

    private readonly string $checkFor;

    private readonly float $percentage;

    /**
     * @param array $params
     */
    public function __construct(
        array $params
    ) {
        $this->percentage = $params['percentage'] ?? null;
        $this->checkOn = $params['checkOn'] ?? $params['check-on'] ?? null;
        if (!in_array($this->checkOn, ['unit_price', 'total', 'quantity'])) {
            throw new LogicException(self::class . ' requires unit_price, total or quantity as value for checkOn');
        }

        $this->checkFor = $params['checkFor'] ?? $params['check-for'] ?? null;
        if (!in_array($this->checkFor, ['min', 'max'])) {
            throw new LogicException(self::class . ' requires min or max as value for checkFor');
        }
    }

    /**
     * @param mixed $orderItems
     * @return DiscountInterface|null
     */
    public function apply(mixed $orderItems): DiscountInterface|null
    {
        if (!$orderItems instanceof OrderItemAggregateCollection) {
            throw new RuntimeException('Expects a collection of order item');
        }

        /** @var OrderItemAggregate $selectedItem */
        $selectedItem = null;

        /** @var OrderItemAggregate $orderItem */
        foreach ($orderItems as $orderItem) {
            if (is_null($selectedItem)) {
                $selectedItem = $orderItem;
                continue;
            }
            if ($this->checkOn === 'total') {
                $checkValue = $orderItem->getOrderItem()->getTotal();
                $compareTo = $selectedItem->getOrderItem()->getTotal();
            } elseif ($this->checkOn === 'quantity') {
                $checkValue = $orderItem->getOrderItem()->getQuantity();
                $compareTo = $selectedItem->getOrderItem()->getQuantity();
            } else { // $this->checkOn === 'unit_price' = default
                $checkValue = $orderItem->getOrderItem()->getUnitPrice();
                $compareTo = $selectedItem->getOrderItem()->getUnitPrice();
            }
            if ($this->checkFor === 'min' && $checkValue < $compareTo) {
                $selectedItem = $orderItem;
            } elseif ($this->checkFor === 'max' && $checkValue > $compareTo) {
                $selectedItem = $orderItem;
            }
        }

        $discount = round($selectedItem->getOrderItem()->getTotalInCents() * ($this->percentage / 100)) / 100;
        if (!$discount) {
            return null;
        }

        return new OrderItemDiscountEntity(
            $selectedItem->getOrderItem()->getProductId(),
            $discount,
            $this->percentage
        );
    }
}
