<?php

namespace Jkeppens\Discounts\Domain\Actions;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Entity\Discount\DiscountInterface;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderDiscountEntity;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregate;
use Jkeppens\Ordering\Domain\Entity\OrderEntity;
use RuntimeException;

class AddOrderValueDiscount implements ActionInterface
{
    private readonly float $discount;

    /**
     * @param array $params
     */
    public function __construct(
        array $params
    ) {
        $this->discount = $params['discount'] ?? null;
    }

    /**
     * @param OrderWithContextAggregate|OrderAggregate|OrderEntity $order
     * @return DiscountInterface|null
     */
    public function apply(mixed $order): DiscountInterface|null
    {
        if ($order instanceof OrderWithContextAggregate) {
            $order = $order->getOrder();
        }
        if ($order instanceof OrderAggregate) {
            $order = $order->getOrder();
        }
        if (!$order instanceof OrderEntity) {
            throw new RuntimeException('Expects an order');
        }

        if (!$this->discount) {
            return null;
        }

        return new OrderDiscountEntity(
            $this->discount,
        );
    }
}
