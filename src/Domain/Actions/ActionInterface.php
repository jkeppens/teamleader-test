<?php

namespace Jkeppens\Discounts\Domain\Actions;

use Jkeppens\Discounts\Domain\Entity\Discount\DiscountInterface;

interface ActionInterface
{
    public function apply(mixed $orderItem): DiscountInterface|null;
}
