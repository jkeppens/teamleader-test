<?php

namespace Jkeppens\Discounts\Domain\Entity\Exception;

use DomainException;

class ConditionMissingDataException extends DomainException
{
    /**
     * @param array $missingFields
     */
    public function __construct(private readonly array $missingFields)
    {
        parent::__construct('DISCOUNTS.DOMAIN.CONDITION.MISSING_FIELDS', 400);
    }

    /**
     * @return array
     */
    public function getMissingFields(): array
    {
        return $this->missingFields;
    }
}
