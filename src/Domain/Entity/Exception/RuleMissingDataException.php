<?php

namespace Jkeppens\Discounts\Domain\Entity\Exception;

use DomainException;

class RuleMissingDataException extends DomainException
{
    /**
     * @param array $missingFields
     */
    public function __construct(private readonly array $missingFields)
    {
        parent::__construct('DISCOUNTS.DOMAIN.RULE.MISSING_FIELDS', 400);
    }

    /**
     * @return array
     */
    public function getMissingFields(): array
    {
        return $this->missingFields;
    }
}
