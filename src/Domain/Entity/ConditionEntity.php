<?php

namespace Jkeppens\Discounts\Domain\Entity;

use Jkeppens\Discounts\Domain\Entity\Exception\ConditionMissingDataException;

class ConditionEntity
{
    public function __construct(
        private readonly string $name,
        private readonly string $class,
        private readonly array $params
    ) {
    }

    public static function fromArray(array $condition): self
    {
        self::checkValidFileTypeArray($condition);
        return new self(
            $condition['name'],
            $condition['class'],
            $condition['params'] ?? []
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name' => $this->getName(),
            'class' => $this->getClass(),
            'params' => $this->getParams(),
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * This is basic validation, it does not check values for being a positive number for example
     *
     * @param array $fileType
     * @return void
     */
    public static function checkValidFileTypeArray(array $fileType): void
    {
        $missingFields = [];
        if (is_null($fileType['name'] ?? null)) {
            $missingFields[] = 'name';
        }
        if (is_null($fileType['class'] ?? null)) {
            $missingFields[] = 'class';
        }
        if (!empty($missingFields)) {
            throw new ConditionMissingDataException($missingFields);
        }
    }
}
