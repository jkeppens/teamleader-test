<?php

namespace Jkeppens\Discounts\Domain\Entity;

use Ramsey\Collection\Collection;

class ActionEntityCollection extends Collection
{
    public function __construct(array $data = [])
    {
        parent::__construct(ActionEntity::class, $data);
    }

    /**
     * @param bool $cascadeArray
     * @return array
     */
    public function toArray(bool $cascadeArray = true): array
    {
        $items = parent::toArray();
        if (!$cascadeArray) {
            return $items;
        }
        return array_map(
            fn (ActionEntity $item) => $item->toArray(),
            $items
        );
    }
}
