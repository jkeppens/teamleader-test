<?php

namespace Jkeppens\Discounts\Domain\Entity\Discount;

interface DiscountInterface
{
    public function toArray(): array;

    public function setReason(string $reason): void;
}
