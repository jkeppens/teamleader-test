<?php

namespace Jkeppens\Discounts\Domain\Entity\Discount;

abstract class AbstractDiscount implements DiscountInterface
{
    private string|null $reason = null;

    /**
     * @return string|null
     */
    public function getReason(): string|null
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }
}
