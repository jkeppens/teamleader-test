<?php

namespace Jkeppens\Discounts\Domain\Entity\Discount;

class OrderDiscountEntity extends AbstractDiscount
{
    public function __construct(
        private readonly float $discount,
        private readonly float|null $percentage = null
    ) {
    }

    /**
     * @return float
     */
    public function getDiscount(): float
    {
        return $this->discount;
    }

    /**
     * @return float|null
     */
    public function getPercentage(): ?float
    {
        return $this->percentage;
    }

    /**
     * @return float[]
     */
    public function toArray(): array
    {
        return [
            'discount' => $this->getDiscount(),
            'percentage' => $this->getPercentage(),
            'reason' => $this->getReason(),
        ];
    }
}
