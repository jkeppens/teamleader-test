<?php

namespace Jkeppens\Discounts\Domain\Entity\Discount;

class OrderItemDiscountEntity extends AbstractDiscount
{
    /**
     * @param string $productId
     * @param float $discount
     * @param float|null $percentage
     */
    public function __construct(
        private readonly string $productId,
        private readonly float $discount,
        private readonly float|null $percentage = null
    ) {
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return float
     */
    public function getDiscount(): float
    {
        return $this->discount;
    }

    /**
     * @return float|null
     */
    public function getPercentage(): float|null
    {
        return $this->percentage;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'product-id' => $this->getProductId(),
            'discount' => $this->getDiscount(),
            'percentage' => $this->getPercentage(),
            'reason' => $this->getReason(),
        ];
    }
}
