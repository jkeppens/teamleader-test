<?php

namespace Jkeppens\Discounts\Domain\Entity\Discount;

use Jkeppens\Discounts\Domain\Enum\DiscountClashResolvementEnum;
use Ramsey\Collection\Collection;
use RuntimeException;

class FreeItemEntityCollection extends Collection
{
    /**
     * @param DiscountClashResolvementEnum $classStrategy
     */
    public function __construct(
        private readonly DiscountClashResolvementEnum $classStrategy
    ) {
        parent::__construct(FreeItemEntity::class);
    }

    /**
     * @param FreeItemEntity $element
     * @return bool
     */
    public function add(mixed $element): bool
    {
        if (!$element instanceof FreeItemEntity) {
            throw new RuntimeException('Expects object of type ' . FreeItemEntity::class);
        }

        /** @var FreeItemEntity $value */
        foreach ($this->data as $key => $value) {
            if ($value->getProductId() == $element->getProductId()) {
                switch ($this->classStrategy) {
                    case DiscountClashResolvementEnum::AddDiscount:
                        $quantity = $value->getQuantity() + $element->getQuantity();
                        break;
                    case DiscountClashResolvementEnum::UseFirstDiscount:
                        $quantity = $value->getQuantity();
                        break;
                    case DiscountClashResolvementEnum::UseLastDiscount:
                        $quantity = $element->getQuantity();
                        break;
                    case DiscountClashResolvementEnum::UseHighestValue:
                        $quantity = max($value->getQuantity(), $element->getQuantity());
                        break;
                    case DiscountClashResolvementEnum::UseLowestValue:
                        $quantity = min($value->getQuantity(), $element->getQuantity());
                        break;
                }
                $this->data[$key] = new FreeItemEntity(
                    $element->getProductId(),
                    $quantity
                );
                return true;
            }
        }

        return parent::add($element);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_map(
            fn (FreeItemEntity $item) => $item->toArray(),
            parent::toArray()
        );
    }
}
