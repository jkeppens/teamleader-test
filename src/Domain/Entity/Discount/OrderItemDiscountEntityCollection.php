<?php

namespace Jkeppens\Discounts\Domain\Entity\Discount;

use Jkeppens\Discounts\Domain\Enum\DiscountClashResolvementEnum;
use Ramsey\Collection\Collection;
use RuntimeException;

class OrderItemDiscountEntityCollection extends Collection
{
    /**
     * @param DiscountClashResolvementEnum $classStrategy
     */
    public function __construct(
        private readonly DiscountClashResolvementEnum $classStrategy
    ) {
        parent::__construct(OrderItemDiscountEntity::class);
    }

    /**
     * @param FreeItemEntity $element
     * @return bool
     */
    public function add(mixed $element): bool
    {
        if (!$element instanceof OrderItemDiscountEntity) {
            throw new RuntimeException('Expects object of type ' . OrderItemDiscountEntity::class);
        }

        /** @var OrderItemDiscountEntity $value */
        foreach ($this->data as $key => $value) {
            if ($value->getProductId() == $element->getProductId()) {
                switch ($this->classStrategy) {
                    case DiscountClashResolvementEnum::AddDiscount:
                        $discount = $value->getDiscount() + $element->getDiscount();
                        $percentage = null;
                        break;
                    case DiscountClashResolvementEnum::UseFirstDiscount:
                        $discount = $value->getDiscount();
                        $percentage = $value->getPercentage();
                        break;
                    case DiscountClashResolvementEnum::UseLastDiscount:
                        $discount = $element->getDiscount();
                        $percentage = $element->getPercentage();
                        break;
                    case DiscountClashResolvementEnum::UseHighestValue:
                        if ($value->getDiscount() < $element->getDiscount()) {
                            $discount = $element->getDiscount();
                            $percentage = $element->getPercentage();
                        } else {
                            $discount = $value->getDiscount();
                            $percentage = $value->getPercentage();
                        }
                        break;
                    case DiscountClashResolvementEnum::UseLowestValue:
                        if ($value->getDiscount() > $element->getDiscount()) {
                            $discount = $element->getDiscount();
                            $percentage = $element->getPercentage();
                        } else {
                            $discount = $value->getDiscount();
                            $percentage = $value->getPercentage();
                        }
                        break;
                }
                $this->data[$key] = new OrderItemDiscountEntity(
                    $element->getProductId(),
                    $discount,
                    $percentage
                );
                return true;
                break;
            }
        }

        return parent::add($element);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_map(
            fn (OrderItemDiscountEntity $item) => $item->toArray(),
            parent::toArray()
        );
    }
}
