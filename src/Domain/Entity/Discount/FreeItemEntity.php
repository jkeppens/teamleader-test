<?php

namespace Jkeppens\Discounts\Domain\Entity\Discount;

class FreeItemEntity extends AbstractDiscount
{
    /**
     * @param string $productId
     * @param int $quantity
     */
    public function __construct(
        private readonly string $productId,
        private readonly int $quantity
    ) {
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'product-id' => $this->getProductId(),
            'quantity' => $this->getQuantity(),
            'reason' => $this->getReason(),
        ];
    }
}
