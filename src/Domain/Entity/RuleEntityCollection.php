<?php

namespace Jkeppens\Discounts\Domain\Entity;

use Ramsey\Collection\Collection;

class RuleEntityCollection extends Collection
{
    public function __construct(array $data = [])
    {
        parent::__construct(RuleEntity::class, $data);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_map(
            fn (RuleEntity $item) => $item->toArray(),
            parent::toArray()
        );
    }
}
