<?php

namespace Jkeppens\Discounts\Domain\Entity;

use Jkeppens\Discounts\Domain\Entity\Exception\RuleMissingDataException;

class RuleEntity
{
    /**
     * @param string $title
     * @param string $condition
     * @param array $conditionParams
     * @param string $action
     * @param array $actionParams
     * @param int|null $id
     */
    public function __construct(
        private string $title,
        private string $condition,
        private array $conditionParams,
        private string $action,
        private array $actionParams,
        private readonly int|null $id
    ) {
    }

    /**
     * @param array $rule
     * @return static
     */
    public static function fromArray(array $rule): self
    {
        self::checkValidRuleArray($rule);
        return new self(
            $rule['title'],
            $rule['condition'],
            $rule['condition-params'] ?? [],
            $rule['action'],
            $rule['action-params'] ?? [],
            $rule['id'] ?? null
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'condition' => $this->getCondition(),
            'condition-params' => $this->getConditionParams(),
            'action' => $this->getAction(),
            'action-params' => $this->getActionParams(),
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCondition(): string
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     */
    public function setCondition(string $condition): void
    {
        $this->condition = $condition;
    }

    /**
     * @return array
     */
    public function getConditionParams(): array
    {
        return $this->conditionParams;
    }

    /**
     * @param array $conditionParams
     */
    public function setConditionParams(array $conditionParams): void
    {
        $this->conditionParams = $conditionParams;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @return array
     */
    public function getActionParams(): array
    {
        return $this->actionParams;
    }

    /**
     * @param array $actionParams
     * @return void
     */
    public function setActionParams(array $actionParams): void
    {
        $this->actionParams = $actionParams;
    }

    /**
     * This is basic validation, it does not check values for being a positive number for example
     *
     * @param array $rule
     * @return void
     */
    public static function checkValidRuleArray(array $rule): void
    {
        $missingFields = [];
        if (empty($rule['title'] ?? null)) {
            $missingFields[] = 'title';
        }
        if (empty($rule['condition'] ?? null)) {
            $missingFields[] = 'condition';
        }
        if (empty($rule['action'] ?? null)) {
            $missingFields[] = 'action';
        }
        if (!empty($missingFields)) {
            throw new RuleMissingDataException($missingFields);
        }
    }
}
