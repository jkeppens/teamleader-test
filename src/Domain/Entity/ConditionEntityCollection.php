<?php

namespace Jkeppens\Discounts\Domain\Entity;

use Ramsey\Collection\Collection;

class ConditionEntityCollection extends Collection
{
    public function __construct(array $data = [])
    {
        parent::__construct(ConditionEntity::class, $data);
    }

    /**
     * @param bool $cascadeArray
     * @return array
     */
    public function toArray(bool $cascadeArray = true): array
    {
        $items = parent::toArray();
        if (!$cascadeArray) {
            return $items;
        }
        return array_map(
            fn (ConditionEntity $item) => $item->toArray(),
            $items
        );
    }
}
