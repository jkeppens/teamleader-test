<?php

namespace Jkeppens\Discounts\Domain\Repository;

use Jkeppens\Discounts\Domain\Entity\RuleEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntityCollection;

interface RuleRepository
{
    /**
     * @param int $id
     * @return RuleEntity|null
     */
    public function find(int $id): ?RuleEntity;

    /**
     * @return RuleEntityCollection
     */
    public function fetchAll(): RuleEntityCollection;

    /**
     * @param RuleEntity $rule
     * @return int
     */
    public function save(RuleEntity $rule): int;

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void;

    /**
     * @return int
     */
    public function getNextId(): int;
}
