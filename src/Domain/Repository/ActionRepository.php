<?php

namespace Jkeppens\Discounts\Domain\Repository;

use Jkeppens\Discounts\Domain\Entity\ActionEntityCollection;
use Jkeppens\Discounts\Domain\Entity\ActionEntity;

interface ActionRepository
{
    /**
     * @param string $name
     * @return ActionEntity|null
     */
    public function find(string $name): ?ActionEntity;

    /**
     * @return ActionEntityCollection
     */
    public function fetchAll(): ActionEntityCollection;
}
