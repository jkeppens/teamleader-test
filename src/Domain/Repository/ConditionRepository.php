<?php

namespace Jkeppens\Discounts\Domain\Repository;

use Jkeppens\Discounts\Domain\Entity\ConditionEntity;
use Jkeppens\Discounts\Domain\Entity\ConditionEntityCollection;

interface ConditionRepository
{
    /**
     * @param string $name
     * @return ConditionEntity|null
     */
    public function find(string $name): ?ConditionEntity;

    /**
     * @return ConditionEntityCollection
     */
    public function fetchAll(): ConditionEntityCollection;
}
