<?php

namespace Jkeppens\Discounts\Domain\Conditions;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;

interface ConditionInterface
{
    public function appliesTo(OrderWithContextAggregate $orderWithContextAggregate);
}
