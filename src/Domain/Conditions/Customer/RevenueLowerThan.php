<?php

namespace Jkeppens\Discounts\Domain\Conditions\Customer;

class RevenueLowerThan extends RevenueBetween
{
    public function __construct(array $params)
    {
        parent::__construct([
            'amountUntil' => $params['amount'] ?? null,
        ]);
    }
}
