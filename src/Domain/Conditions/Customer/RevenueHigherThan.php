<?php

namespace Jkeppens\Discounts\Domain\Conditions\Customer;

class RevenueHigherThan extends RevenueBetween
{
    public function __construct(array $params)
    {
        parent::__construct([
            'amountFrom' => $params['amount'] ?? null,
        ]);
    }
}
