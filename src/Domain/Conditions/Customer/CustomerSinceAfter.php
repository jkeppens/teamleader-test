<?php

namespace Jkeppens\Discounts\Domain\Conditions\Customer;

use Carbon\Carbon;
use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\AbstractCondition;

class CustomerSinceAfter extends CustomerSinceBetween
{
    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        parent::__construct([
            'dateFrom' => $params['date'] ?? null
        ]);
    }
}
