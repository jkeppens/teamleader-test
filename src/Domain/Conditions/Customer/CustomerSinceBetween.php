<?php

namespace Jkeppens\Discounts\Domain\Conditions\Customer;

use Carbon\Carbon;
use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;

class CustomerSinceBetween
{
    private readonly Carbon|null $dateFrom;

    private readonly Carbon|null $dateUntil;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->dateFrom = $params['dateFrom'] ?? $params['date-from'] ?? null;
        $this->dateUntil = $params['dateUntil'] ?? $params['date-until'] ?? null;
    }

    /**
     * Check if the customer became a customer between two dates
     *
     * @param OrderWithContextAggregate $order
     * @return OrderWithContextAggregate|null
     */
    public function appliesTo(OrderWithContextAggregate $order): OrderWithContextAggregate|null
    {
        $customerSince = $order->getCustomer()->getSince();
        if (empty($this->dateFrom) && empty($this->dateUntil)) {
            return $order;
        } elseif (empty($this->dateFrom)) {
            return $customerSince->isBefore($this->dateUntil) ? $order : null;
        } elseif (empty($this->dateUntil)) {
            return $customerSince->isAfter($this->dateFrom) ? $order : null;
        } else {
            return $customerSince->isBetween($this->dateFrom, $this->dateUntil) ? $order : null;
        }
    }
}
