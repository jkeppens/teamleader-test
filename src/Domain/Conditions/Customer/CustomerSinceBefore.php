<?php

namespace Jkeppens\Discounts\Domain\Conditions\Customer;

use Carbon\Carbon;
use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;

class CustomerSinceBefore extends CustomerSinceBetween
{
    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        parent::__construct([
            'dateUntil' => $params['date'] ?? null
        ]);
    }
}
