<?php

namespace Jkeppens\Discounts\Domain\Conditions\OrderItems;

class ForNumberOfProductsInSameCategoryLowerThan extends ForNumberOfProductsInSameCategoryBetween
{
    public function __construct(array $params)
    {
        parent::__construct([
            'categoryId' => $params['categoryId'] ?? $params['category-id'] ?? null,
            'quantityUntil' => $params['quantity'] ?? null,
        ]);
    }
}
