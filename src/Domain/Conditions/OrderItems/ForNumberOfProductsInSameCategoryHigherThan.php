<?php

namespace Jkeppens\Discounts\Domain\Conditions\OrderItems;

class ForNumberOfProductsInSameCategoryHigherThan extends ForNumberOfProductsInSameCategoryBetween
{
    public function __construct(array $params)
    {
        parent::__construct([
            'categoryId' => $params['categoryId'] ?? $params['category-id'] ?? null,
            'quantityFrom' => $params['quantity'] ?? null,
        ]);
    }
}
