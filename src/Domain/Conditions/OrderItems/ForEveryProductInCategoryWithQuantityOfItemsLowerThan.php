<?php

namespace Jkeppens\Discounts\Domain\Conditions\OrderItems;

class ForEveryProductInCategoryWithQuantityOfItemsLowerThan extends ForEveryProductInCategoryWithQuantityOfItemsBetween
{
    public function __construct(array $params)
    {
        parent::__construct([
            'categoryId' => $params['categoryId'] ?? $params['category-id'] ?? null,
            'quantityUntil' => $params['quantity'] ?? null,
        ]);
    }
}
