<?php

namespace Jkeppens\Discounts\Domain\Conditions\OrderItems;

class ForEveryProductInCategoryWithQuantityOfItemsExactly extends ForEveryProductInCategoryWithQuantityOfItemsBetween
{
    public function __construct(array $params)
    {
        parent::__construct([
            'categoryId' => $params['categoryId'] ?? $params['category-id'] ?? null,
            'quantityFrom' => $params['quantity'] ?? null,
            'quantityUntil' => $params['quantity'] ?? null,
        ]);
    }
}
