<?php

namespace Jkeppens\Discounts\Domain\Conditions\OrderItems;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\ConditionInterface;
use Jkeppens\Ordering\Domain\Aggregate\OrderItemAggregate;

class ForEveryProductInCategoryWithQuantityOfItemsBetween implements ConditionInterface
{
    private readonly int $categoryId;

    private readonly int $quantityFrom;

    private readonly int|null $quantityUntil;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->categoryId = $params['categoryId'] ?? $params['category-id'] ?? null;
        $this->quantityFrom = $params['quantityFrom'] ?? $params['quantity-from'] ?? 0;
        $this->quantityUntil = $params['quantityUntil'] ?? $params['quantity-until'] ?? null;
    }

    /**
     * @param OrderWithContextAggregate $orderWithContextAggregate
     * @return array
     */
    public function appliesTo(OrderWithContextAggregate $orderWithContextAggregate): array
    {
        return $orderWithContextAggregate->getOrder()->getItems()->filter(
            function (OrderItemAggregate $item) {
                $quantity = $item->getOrderItem()->getQuantity();
                return ($item->getProduct()->getCategory() == $this->categoryId)
                    && ($quantity >= $this->quantityFrom)
                    && (is_null($this->quantityUntil) || ($quantity <= $this->quantityUntil));
            }
        )->toArray();
    }
}
