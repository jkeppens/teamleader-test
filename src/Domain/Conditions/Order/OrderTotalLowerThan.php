<?php

namespace Jkeppens\Discounts\Domain\Conditions\Order;

class OrderTotalLowerThan extends OrderTotalBetween
{
    public function __construct(array $params)
    {
        parent::__construct([
            'amountUntil' => $params['amount'] ?? null,
        ]);
    }
}
