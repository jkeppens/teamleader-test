<?php

namespace Jkeppens\Discounts\Domain\Conditions\Order;

class OrderTotalHigherThan extends OrderTotalBetween
{
    public function __construct(array $params)
    {
        parent::__construct([
            'amountFrom' => $params['amount'] ?? null,
        ]);
    }
}
