<?php

namespace Jkeppens\Discounts\Domain\Conditions\Order;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\ConditionInterface;

class OrderTotalBetween implements ConditionInterface
{
    private readonly float|null $amountFrom;
    private readonly float|null $amountUntil;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->amountFrom = $params['amountFrom'] ?? $params['amount-from'] ?? 0;
        $this->amountUntil = $params['amountUntil'] ?? $params['amount-until'] ?? null;
    }

    /**
     * Check if the customer became a customer between two dates
     *
     * @param OrderWithContextAggregate $order
     * @return OrderWithContextAggregate|null
     */
    public function appliesTo(OrderWithContextAggregate $order): OrderWithContextAggregate|null
    {
        $total = $order->getOrder()->getOrder()->getTotal();
        $applies = $total >= $this->amountFrom;
        if (!$applies) { // we are already false, or we have no until, return current result
            return null;
        }
        if (is_null($this->amountUntil) || $total <= $this->amountUntil) {
            return $order;
        }
        return null;
    }
}
