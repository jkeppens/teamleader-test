<?php

namespace Jkeppens\Discounts\Domain\Conditions;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;

abstract class AbstractCondition implements ConditionInterface
{
    /**
     * @param array $params
     */
    public function __construct(
        protected readonly array $params
    ) {
    }
}
