<?php

namespace Jkeppens\Discounts\Infrastructure\File\Json;

use Jkeppens\Discounts\Domain\Entity\RuleEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntityCollection;
use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use Jkeppens\Discounts\Infrastructure\File\Json\Exception\RuleDataFileNotFoundException;

class RuleFileRepository implements RuleRepository
{
    /**
     * @param string $dataFile
     */
    public function __construct(
        private readonly string $dataFile
    ) {
        if (!file_exists($this->dataFile)) {
            throw new RuleDataFileNotFoundException();
        }
    }

    /**
     * @param int $id
     * @return RuleEntity|null
     */
    public function find(int $id): ?RuleEntity
    {
        $rules = array_map(
            fn ($rule) => RuleEntity::fromArray($rule),
            array_filter(
                $this->readData(),
                fn ($record) => ($record['id'] == $id)
            )
        );

        if (empty($rules)) {
            return null;
        }

        return reset($rules);
    }

    /**
     * @return RuleEntityCollection
     */
    public function fetchAll(): RuleEntityCollection
    {
        return new RuleEntityCollection(
            array_map(
                fn (array $rule) => RuleEntity::fromArray($rule),
                $this->readData()
            )
        );
    }

    /**
     * @param RuleEntity $rule
     * @return bool
     */
    public function save(RuleEntity $rule): int
    {
        $data = $this->readData(false);

        $found = false;
        $ruleId = $rule->getId() ?? null;
        if (!empty($rule->getId())) {
            /** @var RuleEntity $rule */
            foreach ($data as $key => $record) {
                if ($record['id'] === $rule->getId()) {
                    $data[$key] = $rule->toArray();
                    $found = true;
                }
            }
        }
        if (!$found) {
            $ruleId = $this->getNextId();
            $record = $rule->toArray();
            $record['id'] = $ruleId;
            $data[] = $record;
        }
        $this->saveData($data);
        return $ruleId;
    }

    /**
     * @param int $ruleId
     * @return void
     */
    public function delete(int $ruleId): void
    {
        $data = $this->readData(false);

        foreach ($data as $key => $rule) {
            if ($rule['id'] === $ruleId) {
                unset($data[$key]);
                break;
            }
        }
        $this->saveData($data);
    }

    /**
     * @return array
     */
    private function readData(): array
    {
        return json_decode(file_get_contents($this->dataFile), true);
    }

    /**
     * @param array $data
     * @return bool
     */
    private function saveData(array $data): bool
    {
        return file_put_contents(
            $this->dataFile,
            json_encode(
                $data,
                JSON_PRETTY_PRINT
            )
        ) !== false;
    }

    /**
     * @return int
     */
    public function getNextId(): int
    {
        $data = $this->readData();
        $maxId = 0;
        foreach ($data as $rule) {
            $ruleId = intval($rule['id']);
            if ($ruleId > $maxId) {
                $maxId = $ruleId;
            }
        }
        return $maxId + 1;
    }
}
