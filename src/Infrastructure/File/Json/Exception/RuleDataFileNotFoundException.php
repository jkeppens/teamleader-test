<?php

namespace Jkeppens\Discounts\Infrastructure\File\Json\Exception;

use RuntimeException;

class RuleDataFileNotFoundException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('DISCOUNTS.INFRASTRUCTURE.RULE.DATA_FILE_NOT_FOUND', 500);
    }
}
