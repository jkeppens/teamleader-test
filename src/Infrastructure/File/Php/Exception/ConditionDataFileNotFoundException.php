<?php

namespace Jkeppens\Discounts\Infrastructure\File\Php\Exception;

use RuntimeException;

class ConditionDataFileNotFoundException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('DISCOUNTS.INFRASTRUCTURE.CONDITION.DATA_FILE_NOT_FOUND', 500);
    }
}
