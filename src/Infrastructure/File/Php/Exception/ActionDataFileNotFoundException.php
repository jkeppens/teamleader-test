<?php

namespace Jkeppens\Discounts\Infrastructure\File\Php\Exception;

use RuntimeException;

class ActionDataFileNotFoundException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('DISCOUNTS.INFRASTRUCTURE.ACTION.DATA_FILE_NOT_FOUND', 500);
    }
}
