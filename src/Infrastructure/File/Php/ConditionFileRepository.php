<?php

namespace Jkeppens\Discounts\Infrastructure\File\Php;

use Jkeppens\Discounts\Domain\Entity\ConditionEntity;
use Jkeppens\Discounts\Domain\Entity\ConditionEntityCollection;
use Jkeppens\Discounts\Domain\Repository\ConditionRepository;
use Jkeppens\Discounts\Infrastructure\File\Php\Exception\ConditionDataFileNotFoundException;

class ConditionFileRepository implements ConditionRepository
{
    /** @var array */
    private array $cachedData = [];

    /**
     * @param string $dataFile
     */
    public function __construct(
        private readonly string $dataFile
    ) {
        if (!file_exists($this->dataFile)) {
            throw new ConditionDataFileNotFoundException();
        }
    }

    /**
     * @param string $name
     * @return ConditionEntity|null
     */
    public function find(string $name): ?ConditionEntity
    {
        $rules = array_map(
            fn ($rule) => ConditionEntity::fromArray($rule),
            array_filter(
                $this->readData(),
                fn ($record) => ($record['name'] == $name)
            )
        );

        if (empty($rules)) {
            return null;
        }

        return reset($rules);
    }

    /**
     * @return ConditionEntityCollection
     */
    public function fetchAll(): ConditionEntityCollection
    {
        return new ConditionEntityCollection(
            array_map(
                fn (array $rule) => ConditionEntity::fromArray($rule),
                $this->readData()
            )
        );
    }

    /**
     * @param bool $useCached
     * @return array
     */
    private function readData(bool $useCached = true): array
    {
        if (!$useCached || empty($this->cachedData)) {
            $this->cachedData = require $this->dataFile;
        }
        return $this->cachedData;
    }
}
