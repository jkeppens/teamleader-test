<?php

namespace Jkeppens\Discounts\Infrastructure\File\Php;

use Jkeppens\Discounts\Domain\Entity\ActionEntity;
use Jkeppens\Discounts\Domain\Entity\ActionEntityCollection;
use Jkeppens\Discounts\Domain\Repository\ActionRepository;
use Jkeppens\Discounts\Infrastructure\File\Php\Exception\ActionDataFileNotFoundException;

class ActionFileRepository implements ActionRepository
{
    /** @var array */
    private array $cachedData = [];

    /**
     * @param string $dataFile
     */
    public function __construct(
        private readonly string $dataFile
    ) {
        if (!file_exists($this->dataFile)) {
            throw new ActionDataFileNotFoundException();
        }
    }

    /**
     * @param string $name
     * @return ActionEntity|null
     */
    public function find(string $name): ?ActionEntity
    {
        $rules = array_map(
            fn ($rule) => ActionEntity::fromArray($rule),
            array_filter(
                $this->readData(),
                fn ($record) => ($record['name'] == $name)
            )
        );

        if (empty($rules)) {
            return null;
        }

        return reset($rules);
    }

    /**
     * @return ActionEntityCollection
     */
    public function fetchAll(): ActionEntityCollection
    {
        return new ActionEntityCollection(
            array_map(
                fn (array $rule) => ActionEntity::fromArray($rule),
                $this->readData()
            )
        );
    }

    /**
     * @param bool $useCached
     * @return array
     */
    private function readData(bool $useCached = true): array
    {
        if (!$useCached || empty($this->cachedData)) {
            $this->cachedData = require $this->dataFile;
        }
        return $this->cachedData;
    }
}
