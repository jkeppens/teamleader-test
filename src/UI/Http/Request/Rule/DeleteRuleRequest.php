<?php

namespace Jkeppens\Discounts\UI\Http\Request\Rule;

use Psr\Http\Message\ServerRequestInterface;

class DeleteRuleRequest
{
    public function __construct(
        private readonly ServerRequestInterface $request
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->request->getAttribute('rule-id');
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
        ];
    }
}
