<?php

namespace Jkeppens\Discounts\UI\Http\Request\Rule;

use Psr\Http\Message\ServerRequestInterface;

class CreateRuleRequest
{
    public function __construct(
        protected readonly ServerRequestInterface $request
    ) {
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->request->getParsedBody()['title'] ?? null;
    }

    /**
     * @return string|null
     */
    public function getCondition(): ?string
    {
        return $this->request->getParsedBody()['condition'] ?? null;
    }

    /**
     * @return array
     */
    public function getConditionParams(): array
    {
        return $this->request->getParsedBody()['condition-params'] ?? [];
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->request->getParsedBody()['action'] ?? null;
    }

    /**
     * @return array
     */
    public function getActionParams(): array
    {
        return $this->request->getParsedBody()['action-params'] ?? [];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'title' => $this->getTitle(),
            'condition' => $this->getCondition(),
            'condition-params' => $this->getConditionParams(),
            'action' => $this->getAction(),
            'action-params' => $this->getActionParams(),
        ];
    }
}
