<?php

namespace Jkeppens\Discounts\UI\Http\Request\Rule;

class ModifyRuleRequest extends CreateRuleRequest
{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->request->getAttribute('rule-id');
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['id' => $this->getId()] + parent::toArray();
    }
}
