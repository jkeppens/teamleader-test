<?php

namespace Jkeppens\Discounts\UI\Http\Handler\Condition;

use Jkeppens\Discounts\Domain\Repository\ConditionRepository;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ListConditionsHandler implements RequestHandlerInterface
{
    public function __construct(
        private readonly ConditionRepository $conditionRepository
    ) {
    }

    /**
     * @param $request
     * @return ResponseInterface
     */
    public function handle($request): ResponseInterface
    {
        return new JsonResponse(
            $this->conditionRepository->fetchAll()->toArray()
        );
    }
}
