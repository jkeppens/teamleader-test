<?php

namespace Jkeppens\Discounts\UI\Http\Handler;

use Jkeppens\Discounts\Application\Service\CalculateDiscountService;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregateFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface;

class CalculateDiscountForOrderHandler implements RequestHandlerInterface
{
    public function __construct(
        private readonly CalculateDiscountService $calculateDiscountService,
        private readonly OrderAggregateFactory $orderAggregateFactory
    ) {
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $order = $this->orderAggregateFactory->fromArray($request->getParsedBody());
        $discounts = $this->calculateDiscountService->calculateDiscount($order);

        return new JsonResponse(
            $discounts->toArray()
        );
    }
}
