<?php

namespace Jkeppens\Discounts\UI\Http\Handler;

use Jkeppens\Discounts\Application\Service\CalculateDiscountService;
use Jkeppens\Ordering\Domain\Repository\OrderRepository;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;

class CalculateDiscountForOrderIdHandler implements RequestHandlerInterface
{
    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly CalculateDiscountService $calculateDiscountService
    ) {
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();
        if (empty($body['order_id'])) {
            throw new RuntimeException('Order ID not set');
        }
        $order = $this->orderRepository->find($body['order_id']);
        $discounts = $this->calculateDiscountService->calculateDiscount($order);

        return new JsonResponse(
            $discounts->toArray()
        );
    }
}
