<?php

namespace Jkeppens\Discounts\UI\Http\Handler\Action;

use Jkeppens\Discounts\Domain\Repository\ActionRepository;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ListActionsHandler implements RequestHandlerInterface
{
    public function __construct(
        private readonly ActionRepository $actionRepository
    ) {
    }

    /**
     * @param $request
     * @return ResponseInterface
     */
    public function handle($request): ResponseInterface
    {
        return new JsonResponse(
            $this->actionRepository->fetchAll()->toArray()
        );
    }
}
