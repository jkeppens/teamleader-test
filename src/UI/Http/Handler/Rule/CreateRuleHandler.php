<?php

namespace Jkeppens\Discounts\UI\Http\Handler\Rule;

use Jkeppens\Discounts\Application\Command\Rule\CreateRuleCommand;
use Jkeppens\Discounts\Application\Event\Rule\RuleCreated;
use Jkeppens\Discounts\Domain\Entity\Exception\RuleMissingDataException;
use Jkeppens\Discounts\UI\Http\Request\Rule\CreateRuleRequest;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\ServerRequest;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

class CreateRuleHandler implements RequestHandlerInterface
{
    public function __construct(
        private readonly CommandBus $commandBus
    ) {
    }

    /**
     * @param ServerRequestInterface $serverRequest
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $serverRequest): ResponseInterface
    {
        $request = new CreateRuleRequest($serverRequest);

        $command = new CreateRuleCommand(
            $request->getTitle() ?? '',
            $request->getCondition() ?? '',
            $request->getAction() ?? '',
            $request->getConditionParams() ?? [],
            $request->getActionParams() ?? []
        );

        /** @var RuleCreated $ruleCreated */
        $ruleCreated = $this->commandBus->handle($command);

        return new JsonResponse([
            'message' => 'discounts.rule.created',
            'rule' => $ruleCreated->getRule()->toArray(),
        ]);
    }
}
