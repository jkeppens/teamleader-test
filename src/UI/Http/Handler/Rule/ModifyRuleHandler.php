<?php

namespace Jkeppens\Discounts\UI\Http\Handler\Rule;

use Jkeppens\Discounts\Application\Command\Rule\ModifyRuleCommand;
use Jkeppens\Discounts\Application\Event\Rule\RuleModified;
use Jkeppens\Discounts\UI\Http\Request\Rule\ModifyRuleRequest;
use Laminas\Diactoros\Response\JsonResponse;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ModifyRuleHandler implements RequestHandlerInterface
{
    public function __construct(
        private readonly CommandBus $commandBus
    ) {
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $request = new ModifyRuleRequest($request);

        if (!$request instanceof ModifyRuleRequest) {
            throw new \RuntimeException(
                'Expected a request of type ' . ModifyRuleRequest::class
            );
        }

        $command = new ModifyRuleCommand(
            $request->getId(),
            $request->getTitle() ?? '',
            $request->getCondition() ?? '',
            $request->getAction() ?? '',
            $request->getConditionParams() ?? [],
            $request->getActionParams() ?? []
        );

        /** @var RuleModified $ruleModified */
        $ruleModified = $this->commandBus->handle($command);

        return new JsonResponse([
            'message' => 'discounts.rule.modified',
            'rule' => $ruleModified->getNewData()->toArray(),
        ]);
    }
}
