<?php

namespace Jkeppens\Discounts\UI\Http\Handler\Rule;

use Jkeppens\Discounts\Application\Command\Rule\DeleteRuleCommand;
use Jkeppens\Discounts\Application\Event\Rule\RuleDeleted;
use Jkeppens\Discounts\UI\Http\Request\Rule\DeleteRuleRequest;
use Laminas\Diactoros\Response\JsonResponse;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DeleteRuleHandler implements RequestHandlerInterface
{
    public function __construct(
        private readonly CommandBus $commandBus
    ) {
    }

    /**
     * @param $request
     * @return ResponseInterface
     */
    public function handle($request): ResponseInterface
    {
        $request = new DeleteRuleRequest($request);

        /** @var RuleDeleted $ruleDeleted */
        $ruleDeleted = $this->commandBus->handle(
            new DeleteRuleCommand($request->getId())
        );

        return new JsonResponse([
            'message' => 'discounts.rule.deleted',
            'rule-id' => $ruleDeleted->getRuleId(),
        ]);
    }
}
