<?php

namespace Jkeppens\Discounts\UI\Http\Handler\Rule;

use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ListRulesHandler implements RequestHandlerInterface
{
    public function __construct(
        private readonly RuleRepository $ruleRepository
    ) {
    }

    /**
     * @param $request
     * @return ResponseInterface
     */
    public function handle($request): ResponseInterface
    {
        return new JsonResponse(
            $this->ruleRepository->fetchAll()->toArray()
        );
    }
}
