<?php

namespace Jkeppens\Discounts\Application\Command\Rule;

class DeleteRuleCommand
{
    public function __construct(
        private readonly int $id
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
        ];
    }
}
