<?php

namespace Jkeppens\Discounts\Application\Command\Rule;

use Jkeppens\Discounts\Application\Event\Rule\RuleCreated;
use Jkeppens\Discounts\Domain\Entity\RuleEntity;
use Jkeppens\Discounts\Domain\Repository\ActionRepository;
use Jkeppens\Discounts\Domain\Repository\ConditionRepository;
use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use RuntimeException;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CreateRuleCommandHandler
{
    /**
     * @param RuleRepository $repository
     * @param ConditionRepository $conditionRepository
     * @param ActionRepository $actionRepository
     * @param EventDispatcher|null $eventDispatcher
     */
    public function __construct(
        private readonly RuleRepository $repository,
        private readonly ConditionRepository $conditionRepository,
        private readonly ActionRepository $actionRepository,
        private readonly EventDispatcher|null $eventDispatcher
    ) {
    }

    /**
     * @param CreateRuleCommand $createRuleCommand
     * @return RuleCreated
     */
    public function __invoke(CreateRuleCommand $createRuleCommand): RuleCreated
    {
        if (!$this->conditionRepository->find($createRuleCommand->getCondition())) {
            throw new RuntimeException('Condition not found', 404);
        }

        if (!$this->actionRepository->find($createRuleCommand->getAction())) {
            throw new RuntimeException('Action not found', 404);
        }

        $data = $createRuleCommand->toArray();
        $data['id'] = $this->repository->getNextId();

        $rule = RuleEntity::fromArray($data);
        $this->repository->save($rule);

        return $this->ruleCreatedSuccessfully($rule);
    }

    /**
     * @param RuleEntity $rule
     * @return RuleCreated
     */
    private function ruleCreatedSuccessfully(RuleEntity $rule): RuleCreated
    {
        $event = new RuleCreated($rule);
        $this->eventDispatcher?->dispatch($event);
        return $event;
    }
}
