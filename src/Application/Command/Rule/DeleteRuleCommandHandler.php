<?php

namespace Jkeppens\Discounts\Application\Command\Rule;

use Jkeppens\Discounts\Application\Event\Rule\RuleDeleted;
use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use RuntimeException;
use Symfony\Component\EventDispatcher\EventDispatcher;

class DeleteRuleCommandHandler
{
    /**
     * @param RuleRepository $repository
     * @param EventDispatcher|null $eventDispatcher
     */
    public function __construct(
        private readonly RuleRepository $repository,
        private readonly EventDispatcher|null $eventDispatcher
    ) {
    }

    /**
     * @param DeleteRuleCommand $deleteRuleCommand
     * @return RuleDeleted
     */
    public function __invoke(DeleteRuleCommand $deleteRuleCommand): RuleDeleted
    {
        $ruleId = $deleteRuleCommand->getId();
        $rule = $this->repository->find($ruleId);
        if (empty($rule)) {
            throw new RuntimeException('Rule not found', 404);
        }

        $this->repository->delete($ruleId);

        return $this->ruleDeletedSuccessfully($ruleId);
    }

    /**
     * @param int $ruleId
     * @return RuleDeleted
     */
    private function ruleDeletedSuccessfully(int $ruleId): RuleDeleted
    {
        $event = new RuleDeleted($ruleId);
        $this->eventDispatcher?->dispatch($event);
        return $event;
    }
}
