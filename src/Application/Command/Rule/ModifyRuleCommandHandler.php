<?php

namespace Jkeppens\Discounts\Application\Command\Rule;

use Jkeppens\Discounts\Application\Event\Rule\RuleModified;
use Jkeppens\Discounts\Domain\Entity\Exception\RuleMissingDataException;
use Jkeppens\Discounts\Domain\Entity\RuleEntity;
use Jkeppens\Discounts\Domain\Repository\ActionRepository;
use Jkeppens\Discounts\Domain\Repository\ConditionRepository;
use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use RuntimeException;
use Symfony\Component\EventDispatcher\EventDispatcher;

class ModifyRuleCommandHandler
{
    /**
     * @param RuleRepository $repository
     * @param ConditionRepository $conditionRepository
     * @param ActionRepository $actionRepository
     * @param EventDispatcher|null $eventDispatcher
     */
    public function __construct(
        private readonly RuleRepository $repository,
        private readonly ConditionRepository $conditionRepository,
        private readonly ActionRepository $actionRepository,
        private readonly EventDispatcher|null $eventDispatcher
    ) {
    }

    /**
     * @param ModifyRuleCommand $modifyRuleCommand
     * @return RuleModified
     */
    public function __invoke(ModifyRuleCommand $modifyRuleCommand): RuleModified
    {
        $oldRule = $this->repository->find($modifyRuleCommand->getId());
        if (empty($oldRule)) {
            throw new RuntimeException('Rule not found', 404);
        }

        if (!$this->conditionRepository->find($modifyRuleCommand->getCondition())) {
            throw new RuntimeException('Condition not found', 404);
        }

        if (!$this->actionRepository->find($modifyRuleCommand->getAction())) {
            throw new RuntimeException('Action not found', 404);
        }

        $data = $modifyRuleCommand->toArray();
        $rule = RuleEntity::fromArray($data);
        $this->repository->save($rule);

        return $this->ruleModifiedSuccessfully($rule, $oldRule);
    }

    /**
     * @param RuleEntity $newRule
     * @param RuleEntity $oldRule
     * @return RuleModified
     */
    public function ruleModifiedSuccessfully(RuleEntity $newRule, RuleEntity $oldRule): RuleModified
    {
        $event = new RuleModified($newRule, $oldRule);
        $this->eventDispatcher?->dispatch($event);
        return $event;
    }
}
