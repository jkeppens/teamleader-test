<?php

namespace Jkeppens\Discounts\Application\Command\Rule;

class CreateRuleCommand
{
    /**
     * @param string $title
     * @param string $condition
     * @param string $action
     * @param array $conditionParams
     * @param array $actionParams
     */
    public function __construct(
        private readonly string $title,
        private readonly string $condition,
        private readonly string $action,
        private readonly array $conditionParams = [],
        private readonly array $actionParams = []
    ) {
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return array
     */
    public function getActionParams(): array
    {
        return $this->actionParams;
    }

    /**
     * @return string
     */
    public function getCondition(): string
    {
        return $this->condition;
    }

    /**
     * @return array
     */
    public function getConditionParams(): array
    {
        return $this->conditionParams;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'title' => $this->getTitle(),
            'condition' => $this->getCondition(),
            'condition-params' => $this->getConditionParams(),
            'action' => $this->getAction(),
            'action-params' => $this->getActionParams(),
        ];
    }
}
