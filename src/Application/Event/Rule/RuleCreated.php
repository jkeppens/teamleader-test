<?php

namespace Jkeppens\Discounts\Application\Event\Rule;

use Jkeppens\Discounts\Application\Command\Rule\CreateRuleCommand;
use Jkeppens\Discounts\Domain\Entity\RuleEntity;

class RuleCreated
{
    public function __construct(
        private readonly RuleEntity $rule
    ) {
    }

    /**
     * @return RuleEntity
     */
    public function getRule(): RuleEntity
    {
        return $this->rule;
    }
}
