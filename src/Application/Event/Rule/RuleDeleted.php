<?php

namespace Jkeppens\Discounts\Application\Event\Rule;

class RuleDeleted
{
    public function __construct(
        private readonly int $ruleId
    ) {
    }

    /**
     * @return int
     */
    public function getRuleId(): int
    {
        return $this->ruleId;
    }
}
