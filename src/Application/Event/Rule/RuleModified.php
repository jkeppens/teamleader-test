<?php

namespace Jkeppens\Discounts\Application\Event\Rule;

use Jkeppens\Discounts\Domain\Entity\RuleEntity;

class RuleModified
{
    public function __construct(
        private readonly RuleEntity $newData,
        private readonly RuleEntity $oldData,
    ) {
    }

    /**
     * @return RuleEntity
     */
    public function getOldData(): RuleEntity
    {
        return $this->oldData;
    }

    /**
     * @return RuleEntity
     */
    public function getNewData(): RuleEntity
    {
        return $this->newData;
    }
}
