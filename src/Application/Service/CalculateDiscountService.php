<?php

namespace Jkeppens\Discounts\Application\Service;

use Jkeppens\Customer\Domain\Repository\CustomerRepository;
use Jkeppens\Discounts\Domain\Aggregate\DiscountAggregate;
use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Aggregate\RulesAggregate;
use Jkeppens\Discounts\Domain\Aggregate\RulesAggregateCollection;
use Jkeppens\Discounts\Domain\Entity\ActionEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntityCollection;
use Jkeppens\Discounts\Domain\Entity\ConditionEntity;
use Jkeppens\Discounts\Domain\Repository\ActionRepository;
use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use Jkeppens\Discounts\Domain\Repository\ConditionRepository;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregate;

class CalculateDiscountService
{
    public function __construct(
        private readonly RuleRepository $ruleRepository,
        private readonly CustomerRepository $customerRepository,
        private readonly ConditionRepository $conditionRepository,
        private readonly ActionRepository $actionRepository,
        private readonly DiscountAggregate $discounts
    ) {
    }

    /**
     * @param OrderAggregate $order
     * @return DiscountAggregate
     */
    public function calculateDiscount(OrderAggregate $order): DiscountAggregate
    {
        $customer = $this->customerRepository->find(
            $order->getOrder()->getCustomerId()
        );

        $orderWithContext = new OrderWithContextAggregate(
            $order,
            $customer
        );

        $rules = $this->calculateRulesAggregateCollection($this->discounts);
        $rules->calculateDiscounts($orderWithContext);
        return $rules->getDiscounts();
    }

    /**
     * @param DiscountAggregate $discounts
     * @return RulesAggregateCollection
     */
    public function calculateRulesAggregateCollection(DiscountAggregate $discounts): RulesAggregateCollection
    {
        $rules = $this->getRules();
        $conditions = $this->getConditions();
        $actions = $this->getActions();
        $collection = new RulesAggregateCollection($discounts);

        /** @var RuleEntity $rule */
        foreach ($rules as $rule) {
            $collection->add(
                $this->calculateRulesAggregate($rule, $conditions, $actions)
            );
        }

        return $collection;
    }

    /**
     * @param RuleEntity $rule
     * @param array $conditions
     * @param array $actions
     * @return RulesAggregate
     */
    protected function calculateRulesAggregate(RuleEntity $rule, array $conditions, array $actions): RulesAggregate
    {
        /** @var ConditionEntity $conditionEntity */
        $conditionEntity = $conditions[$rule->getCondition()];
        $class = $conditionEntity->getClass();
        $condition = new $class($rule->getConditionParams());

        /** @var ActionEntity $actionEntity */
        $actionEntity = $actions[$rule->getAction()];
        $class = $actionEntity->getClass();
        $action = new $class($rule->getActionParams());

        return new RulesAggregate(
            $rule,
            $conditionEntity,
            $actionEntity,
            $condition,
            $action
        );
    }

    /**
     * @return RuleEntityCollection
     */
    public function getRules(): RuleEntityCollection
    {
        return $this->ruleRepository->fetchAll();
    }

    /**
     * @return array
     */
    public function getConditions(): array
    {
        $values = $this->conditionRepository->fetchAll()->toArray(false);
        $keys = array_map(
            fn (ConditionEntity $entity) => $entity->getName(),
            $values
        );
        return array_combine($keys, $values);
    }

    /**
     * @return array
     */
    public function getActions(): array
    {
        $values = $this->actionRepository->fetchAll()->toArray(false);
        $keys = array_map(
            fn (ActionEntity $entity) => $entity->getName(),
            $values
        );
        return array_combine($keys, $values);
    }
}
