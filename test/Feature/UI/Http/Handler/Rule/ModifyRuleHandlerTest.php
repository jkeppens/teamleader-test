<?php

namespace JkeppensTest\Discounts\Feature\UI\Http\Handler\Action;

use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use JkeppensTest\Discounts\Feature\AbstractFeatureTest;
use Laminas\Diactoros\ServerRequest;

class ModifyRuleHandlerTest extends AbstractFeatureTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->cacheRulesDb();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->resetRulesDb();
    }

    public function testModifyRuleValid(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules/1',
            method: 'PUT',
            parsedBody: [
                'title' => 'when-customer-revenue-over-500-give-15-percent-discount-on-order',
                'condition' => 'order-total-higher-than',
                'condition-params' => [
                    'amount' => 500
                ],
                'action' => 'add-order-item-percentage-discount',
                'action-params' => [
                    'percentage' => 15
                ],
            ]
        );

        $response = $this->app->handle($request);
        self::assertThat($response, self::isSuccess());
        $actual = json_decode($response->getBody(), true);
        self::assertEquals('discounts.rule.modified', $actual['message']);
        self::assertNotEmpty($actual['rule']['id']);
        self::assertEquals('when-customer-revenue-over-500-give-15-percent-discount-on-order', $actual['rule']['title']);

        /** @var RuleRepository $ruleRepository */
        $ruleRepository = $this->container->get(RuleRepository::class);
        $stored = $ruleRepository->find(1);

        self::assertEquals(
            'when-customer-revenue-over-500-give-15-percent-discount-on-order',
            $stored->getTitle()
        );

        self::assertEquals(
            ['amount' => 500],
            $stored->getConditionParams()
        );

        self::assertEquals(
            ['percentage' => 15],
            $stored->getActionParams()
        );
    }

    public function testModifyWithoutTitle(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules/1',
            method: 'PUT',
            parsedBody: [
                'condition' => 'order-total-higher-than',
                'condition-params' => [
                    'amount' => 500
                ],
                'action' => 'add-order-item-percentage-discount',
                'action-params' => [
                    'percentage' => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(400));
    }

    public function testModifyWithInvalidCondition(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules/1',
            method: 'PUT',
            parsedBody: [
                'title' => 'this-is-a-test',
                'condition' => 'order-total-higher-than-does-not-exist',
                'condition-params' => [
                    'amount' => 500
                ],
                'action' => 'add-order-item-percentage-discount',
                'action-params' => [
                    'percentage' => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(404));
    }

    public function testModifyWithoutCondition(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules/1',
            method: 'PUT',
            parsedBody: [
                'title' => 'this-is-a-test',
                'condition-params' => [
                    'amount' => 500
                ],
                'action' => 'add-order-item-percentage-discount',
                'action-params' => [
                    'percentage' => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(404));
    }

    public function testModifyWithInvalidAction(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules/1',
            method: 'PUT',
            parsedBody: [
                'title' => 'this-is-a-test',
                'condition' => 'order-total-higher-than',
                'condition-params' => [
                    'amount' => 500
                ],
                'action' => 'add-order-item-percentage-discount-does-not-exist',
                'action-params' => [
                    'percentage' => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(404));
    }

    public function testModifyWithoutAction(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules/1',
            method: 'PUT',
            parsedBody: [
                'title' => 'this-is-a-test',
                'condition-params' => [
                    'amount' => 500
                ],
                'action' => 'add-order-item-percentage-discount-does-not-exist',
                'action-params' => [
                    'percentage' => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(404));
    }
}
