<?php

namespace JkeppensTest\Discounts\Feature\UI\Http\Handler\Action;

use JkeppensTest\Discounts\Feature\AbstractFeatureTest;
use Laminas\Diactoros\ServerRequest;

class CreateRuleHandlerTest extends AbstractFeatureTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->cacheRulesDb();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->resetRulesDb();
    }

    public function testCreateValidRule(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules',
            method: 'POST',
            parsedBody: [
                "title" => "this-is-a-test",
                "condition" => "order-total-higher-than",
                "condition-params" => [
                    "amount" => 500
                ],
                "action" => "add-order-item-percentage-discount",
                "action-params" => [
                    "percentage" => 5
                ],
            ]
        );

        $response = $this->app->handle($request);
        self::assertThat($response, self::isSuccess());
        $actual = json_decode($response->getBody(), true);
        self::assertEquals('discounts.rule.created', $actual['message']);
        self::assertNotEmpty($actual['rule']['id']);
    }

    public function testCreateWithoutTitle(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules',
            method: 'POST',
            parsedBody: [
                "condition" => "order-total-higher-than",
                "condition-params" => [
                    "amount" => 500
                ],
                "action" => "add-order-item-percentage-discount",
                "action-params" => [
                    "percentage" => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(400));
    }

    public function testCreateWithInvalidCondition(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules',
            method: 'POST',
            parsedBody: [
                "title" => "this-is-a-test",
                "condition" => "order-total-higher-than-does-not-exist",
                "condition-params" => [
                    "amount" => 500
                ],
                "action" => "add-order-item-percentage-discount",
                "action-params" => [
                    "percentage" => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(404));
    }

    public function testCreateWithoutCondition(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules',
            method: 'POST',
            parsedBody: [
                "title" => "this-is-a-test",
                "condition-params" => [
                    "amount" => 500
                ],
                "action" => "add-order-item-percentage-discount",
                "action-params" => [
                    "percentage" => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(404));
    }

    public function testCreateWithInvalidAction(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules',
            method: 'POST',
            parsedBody: [
                "title" => "this-is-a-test",
                "condition" => "order-total-higher-than",
                "condition-params" => [
                    "amount" => 500
                ],
                "action" => "add-order-item-percentage-discount-does-not-exist",
                "action-params" => [
                    "percentage" => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(404));
    }

    public function testCreateWithoutAction(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules',
            method: 'POST',
            parsedBody: [
                "title" => "this-is-a-test",
                "condition-params" => [
                    "amount" => 500
                ],
                "action" => "add-order-item-percentage-discount-does-not-exist",
                "action-params" => [
                    "percentage" => 5
                ],
            ]
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::hasStatus(404));
    }
}
