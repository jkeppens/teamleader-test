<?php

namespace JkeppensTest\Discounts\Feature\UI\Http\Handler\Action;

use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use JkeppensTest\Discounts\Feature\AbstractFeatureTest;
use Laminas\Diactoros\ServerRequest;

class DeleteRuleHandlerTest extends AbstractFeatureTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->cacheRulesDb();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->resetRulesDb();
    }

    public function testDeleteRuleValid(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules/1',
            method: 'DELETE'
        );

        $response = $this->app->handle($request);
        self::assertThat($response, self::isSuccess());
        $actual = json_decode($response->getBody(), true);
        self::assertEquals('discounts.rule.deleted', $actual['message']);

        /** @var RuleRepository $ruleRepository */
        $ruleRepository = $this->container->get(RuleRepository::class);
        $stored = $ruleRepository->find(1);

        self::assertEmpty($stored);
    }
}
