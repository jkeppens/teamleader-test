<?php

namespace JkeppensTest\Discounts\Feature\UI\Http\Handler\Action;

use JkeppensTest\Discounts\Feature\AbstractFeatureTest;
use Laminas\Diactoros\ServerRequest;
use PHPUnit\Framework\Constraint\IsIdentical;

class ListRulesHandlerTest extends AbstractFeatureTest
{
    public function testLoadActions(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/rules',
            method: 'GET'
        );

        $actualData = file_get_contents($this->root . '/data/rules.json');

        $response = $this->app->handle($request);
        self::assertThat($response, self::isSuccess());
        self::assertThat(
            json_decode($response->getBody(), true),
            new IsIdentical(json_decode($actualData, true))
        );
    }
}
