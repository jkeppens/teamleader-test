<?php

namespace JkeppensTest\Discounts\Feature\UI\Http\Handler\Action;

use JkeppensTest\Discounts\Feature\AbstractFeatureTest;
use Laminas\Diactoros\ServerRequest;
use PHPUnit\Framework\Constraint\IsIdentical;

class ListConditionsHandlerTest extends AbstractFeatureTest
{
    public function testLoadActions(): void
    {
        $request = new ServerRequest(
            uri: '/api/discounts/conditions',
            method: 'GET'
        );

        $actualData = require $this->root . '/../data/php/database/conditions.php';

        $response = $this->app->handle($request);
        self::assertThat($response, self::isSuccess());
        self::assertThat(
            json_decode($response->getBody(), true),
            new IsIdentical($actualData)
        );
    }
}
