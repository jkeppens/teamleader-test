<?php

namespace JkeppensTest\Discounts\Feature\UI\Http\Handler;

use JkeppensTest\Discounts\Feature\AbstractFeatureTest;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\ServerRequestFactory;

class CalculateDiscountForOrderIdHandlerTest extends AbstractFeatureTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->cacheRulesDb();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->resetRulesDb();
    }

    public function testOrderWithCustomerLowRevenue2FreeProducts(): void
    {
        $request = new ServerRequest(
            uri: '/api/calculate-discount-for-order-id',
            method: 'POST',
            parsedBody: [
                'order_id' => 1
            ],
        );

        $response = $this->app->handle($request);

        self::assertThat($response, self::isSuccess());
        $actual = json_decode($response->getBody(), true);

        $expected = [
            "free-items" => [
                [
                    "product-id" => "B102",
                    "quantity" => 2,
                    "reason" => "for-every-5-items-bought-for-products-in-category-switches-give-1-item-free",
                ],
            ],
        ];

        self::assertArrayNotHasKey('order', $actual);
        self::assertArrayNotHasKey('order-items', $actual);
        self::assertArrayHasKey('free-items', $actual);

        self::assertEquals($expected['free-items'], $actual['free-items']);
    }

    public function testOrderWithCustomerHighRevenue1FreeProducts2(): void
    {
        $request = new ServerRequest(
            uri: '/api/calculate-discount-for-order-id',
            method: 'POST',
            parsedBody: [
                'order_id' => 2
            ],
        );

        $response = $this->app->handle($request);
        self::assertThat($response, self::isSuccess());
        $actual = json_decode($response->getBody(), true);

        $expected = [
            "order" => [
                "discount" => 2.5,
                "percentage" => 10,
                "reason" => "when-customer-revenue-over-1000-give-10-percent-discount-on-order",
            ],
            "free-items" => [
                [
                    "product-id" => "B102",
                    "quantity" => 1,
                    "reason" => "for-every-5-items-bought-for-products-in-category-switches-give-1-item-free",
                ],
            ],
        ];

        self::assertArrayNotHasKey('order-items', $actual);

        self::assertArrayHasKey('order', $actual);
        self::assertEquals($expected['order'], $actual['order']);

        self::assertArrayHasKey('free-items', $actual);
        self::assertEquals($expected['free-items'], $actual['free-items']);
    }

    public function testOrderWithCustomerLogRevenueDiscountOnLowestToolProduct(): void
    {
        $request = new ServerRequest(
            uri: '/api/calculate-discount-for-order-id',
            method: 'POST',
            parsedBody: [
                'order_id' => 3
            ],
        );

        $response = $this->app->handle($request);
        self::assertThat($response, self::isSuccess());
        $actual = json_decode($response->getBody(), true);

        $expected = [
            "order-items" => [
                [
                    "product-id" => "A101",
                    "discount" => 3.9,
                    "percentage" => 20,
                    "reason" => "for-2-or-more-products-boughts-in-tools-give-20-percent-discount-on-cheapest-product"
                ],
            ],
        ];

        self::assertArrayNotHasKey('order', $actual);
        self::assertArrayNotHasKey('free-items', $actual);

        self::assertArrayHasKey('order-items', $actual);
        self::assertEquals($expected['order-items'], $actual['order-items']);
    }
}
