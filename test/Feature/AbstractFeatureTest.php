<?php

namespace JkeppensTest\Discounts\Feature;

use Helmich\JsonAssert\Constraint\JsonValueMatchesMany;
use Helmich\Psr7Assert\Psr7Assertions;
use Mezzio\Application;
use Mezzio\MiddlewareFactory;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Constraint\Constraint;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

abstract class AbstractFeatureTest extends TestCase
{
    use Psr7Assertions;

    protected ContainerInterface $container;

    protected Application $app;

    protected string $root;

    private string|null $cachedRules = null;

    /**
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->initContainer();
        $this->initApp();
        $this->initPipeline();
        $this->initRoutes();

        $this->root = realpath(__DIR__ . '/../');
    }

    /**
     * @return void
     */
    protected function initContainer(): void
    {
        $isInFeatureTest = true;
        $this->container = require __DIR__ . '/../../config/container.php';
    }

    /**
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function initApp(): void
    {
        $this->app = $this->container->get(Application::class);
    }

    /**
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function initPipeline(): void
    {
        $factory = $this->container->get(MiddlewareFactory::class);
        (require __DIR__ . '/../../config/pipeline.php')($this->app, $factory, $this->container);
    }

    /**
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function initRoutes(): void
    {
        $factory = $this->container->get(MiddlewareFactory::class);
        (require __DIR__ . '/../../config/routes.php')($this->app, $factory, $this->container);
    }

    /**
     * @param array $constraints
     * @return Constraint
     */
    public static function bodyMatchesJson(array $constraints): Constraint
    {
        return Assert::logicalAnd(
            self::hasHeader(
                'content-type',
                Assert::matchesRegularExpression(
                    ',^application/(.+\+)?json(;.+)?$,'
                )
            ),
            self::bodyMatches(
                Assert::logicalAnd(
                    Assert::isJson(),
                    new JsonValueMatchesMany($constraints)
                )
            )
        );
    }

    /**
     * @return void
     */
    protected function cacheRulesDb(): void
    {
        $this->cachedRules = file_get_contents($this->root . '/data/rules.json');
    }

    /**
     * @return void
     */
    protected function resetRulesDb(): void
    {
        if (!empty($this->cachedRules)) {
            file_put_contents($this->root . '/data/rules.json', $this->cachedRules);
        }
    }
}
