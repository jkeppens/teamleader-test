<?php

namespace JkeppensTest\Discounts\Feature;

class ConfigProvider
{
    /**
     * @return array
     */
    public function __invoke()
    {
        return [
            'database' => [
                'json' => [
                    'files' => [
                        'rules' => __DIR__ . '/../data/rules.json',
                    ],
                ],
            ],
        ];
    }
}
