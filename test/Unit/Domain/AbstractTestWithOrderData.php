<?php

namespace JkeppensTest\Discounts\Unit\Domain;

use Jkeppens\Customer\Domain\Entity\CustomerEntity;
use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Ordering\Domain\Aggregate\OrderAggregate;
use PHPUnit\Framework\TestCase;

abstract class AbstractTestWithOrderData extends TestCase
{
    /**
     * @param int $orderId
     * @return OrderWithContextAggregate
     */
    protected function getTestOrderWithCustomer(int $orderId): OrderWithContextAggregate
    {
        if ($orderId === 2) {
            $customer = CustomerEntity::fromArray([
                "id" => 2,
                "name" => "Teamleader",
                "since" => "2015-01-15",
                "revenue" => 1505.95,
            ]);

            $order = OrderAggregate::fromArray([
                "id" => 2,
                "customer-id" => 2,
                "items" => [
                    [
                        "product-id" => "B102",
                        "quantity" => 5,
                        "unit-price" => 4.99,
                        "total" => 24.95,
                        "product" => [
                            "id" => "B102",
                            "description" => "Press button",
                            "category" => 2,
                            "price" => 4.99,
                        ],
                    ],
                ],
                "total" => 24.95
            ]);
        } elseif ($orderId === 3) {
            $customer = CustomerEntity::fromArray([
                "id" => 3,
                "name" => "Jeroen De Wit",
                "since" => "2016-02-11",
                "revenue" => 0.0,
            ]);

            $order = OrderAggregate::fromArray([
                "id" => 3,
                "customer-id" => 3,
                "items" => [
                    [
                        "product-id" => "A101",
                        "quantity" => 2,
                        "unit-price" => 9.75,
                        "total" => 19.50,
                        "product" => [
                            "id" => "A101",
                            "description" => "Screwdriver",
                            "category" => 1,
                            "price" => 9.75,
                        ],
                    ],
                    [
                        "product-id" => "A102",
                        "quantity" => 1,
                        "unit-price" => 49.50,
                        "total" => 49.50,
                        "product" => [
                            "id" => "A102",
                            "description" => "Electric screwdriver",
                            "category" => 1,
                            "price" => 49.5,
                        ],
                    ],
                ],
                "total" => 69.00
            ]);
        } else {
            $customer = CustomerEntity::fromArray([
                "id" => 1,
                "name" => "Coca Cola",
                "since" => "2020-06-28",
                "revenue" => 492.12,
            ]);

            $order = OrderAggregate::fromArray([
                "id" => 1,
                "customer-id" => 1,
                "items" => [
                    [
                        "product-id" => "B102",
                        "quantity" => 10,
                        "unit-price" => 4.99,
                        "total" => 49.90,
                        "product" => [
                            "id" => "B102",
                            "description" => "Press button",
                            "category" => 2,
                            "price" => 4.99,
                        ],
                    ],
                ],
                "total" => 49.90
            ]);
        }

        return new OrderWithContextAggregate(
            $order,
            $customer
        );
    }
}