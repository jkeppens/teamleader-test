<?php

namespace JkeppensTest\Discounts\Unit\Domain\Aggregate;

use Jkeppens\Discounts\Domain\Aggregate\DiscountAggregate;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderDiscountEntity;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntity;
use Jkeppens\Discounts\Domain\Enum\DiscountClashResolvementEnum;
use PHPUnit\Framework\TestCase;

class DiscountAggregateTest extends TestCase
{
    public function orderDiscountDataProvider(): array
    {
        return [
            [
                DiscountClashResolvementEnum::UseHighestValue,
                [
                    new OrderDiscountEntity(10, null),
                    new OrderDiscountEntity(10, 5),
                    new OrderDiscountEntity(8, 10),
                ],
                10,
                null,
            ],
            [
                DiscountClashResolvementEnum::UseHighestValue,
                [
                    new OrderDiscountEntity(10, null),
                    new OrderDiscountEntity(10, 5),
                    new OrderDiscountEntity(8, 10),
                    new OrderDiscountEntity(12, 10),
                ],
                12,
                10,
            ],
            [
                DiscountClashResolvementEnum::UseLowestValue,
                [
                    new OrderDiscountEntity(10, null),
                    new OrderDiscountEntity(8, 10),
                    new OrderDiscountEntity(8, null),
                ],
                8,
                10,
            ],
            [
                DiscountClashResolvementEnum::UseLowestValue,
                [
                    new OrderDiscountEntity(8, 10),
                    new OrderDiscountEntity(8, null),
                    new OrderDiscountEntity(10, null),
                    new OrderDiscountEntity(3, null),
                ],
                3,
                null,
            ],
            [
                DiscountClashResolvementEnum::UseFirstDiscount,
                [
                    new OrderDiscountEntity(10, null),
                    new OrderDiscountEntity(8, 10),
                    new OrderDiscountEntity(8, null),
                ],
                10,
                null,
            ],
            [
                DiscountClashResolvementEnum::UseLastDiscount,
                [
                    new OrderDiscountEntity(10, null),
                    new OrderDiscountEntity(8, 10),
                    new OrderDiscountEntity(8, null),
                ],
                8,
                null,
            ],
            [
                DiscountClashResolvementEnum::AddDiscount,
                [
                    new OrderDiscountEntity(10, null),
                    new OrderDiscountEntity(8, 10),
                    new OrderDiscountEntity(8, null),
                ],
                26,
                null,
            ],
        ];
    }

    /**
     * @dataProvider orderDiscountDataProvider
     * @param DiscountClashResolvementEnum $strategy
     * @param array $discountsToAdd
     * @param float|null $expectedDiscount
     * @param float|null $expectedPercentage
     * @return void
     */
    public function testAddOrderDiscount(
        DiscountClashResolvementEnum $strategy,
        array $discountsToAdd,
        float|null $expectedDiscount,
        float|null $expectedPercentage
    ): void
    {
        $aggregator = new DiscountAggregate($strategy, $strategy, $strategy);

        foreach ($discountsToAdd as $discount) {
            $aggregator->addOrderDiscount($discount);
        }

        $this->assertEquals($expectedDiscount, $aggregator->getOrderDiscount()->getDiscount());
        $this->assertEquals($expectedPercentage, $aggregator->getOrderDiscount()->getPercentage());

        $this->assertEquals(0, $aggregator->getFreeItems()->count());
        $this->assertEquals(0, $aggregator->getOrderItemDiscounts()->count());
    }

    public function orderItemDiscountDataProvider(): array
    {
        $items = [
            new OrderItemDiscountEntity('A101', 10, null),
            new OrderItemDiscountEntity('A102', 5, null),
            new OrderItemDiscountEntity('A101', 10, 5),
            new OrderItemDiscountEntity('A101', 8, 10),
            new OrderItemDiscountEntity('A102', 10, 3),
        ];
        return [
            [
                DiscountClashResolvementEnum::UseHighestValue,
                $items,
                2,
                'A101',
                10,
                null,
                'A102',
                10,
                3,
            ],
            [
                DiscountClashResolvementEnum::UseLowestValue,
                $items,
                2,
                'A101',
                8,
                10,
                'A102',
                5,
                null,
            ],
            [
                DiscountClashResolvementEnum::UseFirstDiscount,
                $items,
                2,
                'A101',
                10,
                null,
                'A102',
                5,
                null,
            ],
            [
                DiscountClashResolvementEnum::UseLastDiscount,
                $items,
                2,
                'A101',
                8,
                10,
                'A102',
                10,
                3,
            ],
            [
                DiscountClashResolvementEnum::AddDiscount,
                $items,
                2,
                'A101',
                28,
                null,
                'A102',
                15,
                null,
            ],
        ];
    }

    /**
     * @dataProvider orderItemDiscountDataProvider
     * @param DiscountClashResolvementEnum $strategy
     * @param array $discountsToAdd
     * @param int $expectedNumberOfProducts
     * @param string|null $expectedProduct1
     * @param float|null $expectedDiscountProduct1
     * @param float|null $expectedPercentageProduct1
     * @param string|null $expectedProduct2
     * @param float|null $expectedDiscountProduct2
     * @param float|null $expectedPercentageProduct2
     * @return void
     */
    public function testAddOrderItemDiscount(
        DiscountClashResolvementEnum $strategy,
        array $discountsToAdd,
        int $expectedNumberOfProducts,
        string|null $expectedProduct1,
        float|null $expectedDiscountProduct1,
        float|null $expectedPercentageProduct1,
        string|null $expectedProduct2,
        float|null $expectedDiscountProduct2,
        float|null $expectedPercentageProduct2
    ): void
    {
        $aggregator = new DiscountAggregate($strategy, $strategy, $strategy);

        foreach ($discountsToAdd as $discount) {
            $aggregator->addOrderItemDiscount($discount);
        }

        $this->assertEquals($expectedNumberOfProducts, $aggregator->getOrderItemDiscounts()->count());

        $discounts = $aggregator->getOrderItemDiscounts();
        for ($i = 0; $i < $expectedNumberOfProducts; $i++) {
            /** @var OrderItemDiscountEntity $discount */
            $discount = $discounts[$i];
            $idx = $i + 1;
            $this->assertEquals(${'expectedProduct' . $idx}, $discount->getProductId());
            $this->assertEquals(${'expectedDiscountProduct' . $idx}, $discount->getDiscount());
            $this->assertEquals(${'expectedPercentageProduct' . $idx}, $discount->getPercentage());
        }
    }
}