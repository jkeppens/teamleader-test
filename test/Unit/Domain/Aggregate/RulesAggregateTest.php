<?php

namespace JkeppensTest\Discounts\Unit\Domain\Aggregate;

use Jkeppens\Discounts\Domain\Actions\ActionInterface;
use Jkeppens\Discounts\Domain\Actions\AddFreeItems;
use Jkeppens\Discounts\Domain\Actions\AddOrderValueDiscount;
use Jkeppens\Discounts\Domain\Aggregate\DiscountAggregate;
use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Aggregate\RulesAggregate;
use Jkeppens\Discounts\Domain\Conditions\ConditionInterface;
use Jkeppens\Discounts\Domain\Conditions\Customer\RevenueHigherThan;
use Jkeppens\Discounts\Domain\Entity\ActionEntity;
use Jkeppens\Discounts\Domain\Entity\ConditionEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntity;
use Jkeppens\Discounts\Domain\Enum\DiscountClashResolvementEnum;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

/**
 * Due to time constraints only Order Items addition has been added to the unit test.
 * In a real application, other cases would be tested as well of course
 */
class RulesAggregateTest extends AbstractTestWithOrderData
{
    public function applyRulesDataProvider(): array
    {
        return [
            [
                $this->getTestOrderWithCustomer(1),
                RuleEntity::fromArray([
                    'id' => 123,
                    'title' => 'add-5-euro-when-revenue-higher-than-200',
                    'condition' => 'customer-revenue-higher-than',
                    'condition-params' => [
                        'amount' => 200
                    ],
                    'action' => 'add-order-value-discount',
                    'action-params' => [
                        'discount' => 5
                    ]
                ]),
                ConditionEntity::fromArray([
                    'name' => 'customer-revenue-higher-than',
                    'class' => RevenueHigherThan::class,
                    'params' => [
                        'amount' => [
                            'required' => true,
                            'type' => 'float',
                        ],
                    ],
                ]),
                ActionEntity::fromArray([
                    'name' => 'add-order-value-discount',
                    'class' => AddOrderValueDiscount::class,
                    'params' => [
                        'discount' => [
                            'required' => true,
                            'type' => 'float',
                        ],
                    ],
                ]),
                new RevenueHigherThan(['amount' => 200]),
                new AddOrderValueDiscount(['discount' => 5]),
                DiscountClashResolvementEnum::UseHighestValue,
                [
                    'order' => [
                        'discount' => 5,
                        'percentage' => null,
                        'reason' => 'add-5-euro-when-revenue-higher-than-200',
                    ],
                ],
            ],
            [
                $this->getTestOrderWithCustomer(1),
                RuleEntity::fromArray([
                    'id' => 123,
                    'title' => 'add-5-euro-when-revenue-higher-than-1200',
                    'condition' => 'customer-revenue-higher-than',
                    'condition-params' => [
                        'amount' => 1200
                    ],
                    'action' => 'add-order-value-discount',
                    'action-params' => [
                        'discount' => 5
                    ]
                ]),
                ConditionEntity::fromArray([
                    'name' => 'customer-revenue-higher-than',
                    'class' => RevenueHigherThan::class,
                    'params' => [
                        'amount' => [
                            'required' => true,
                            'type' => 'float',
                        ],
                    ],
                ]),
                ActionEntity::fromArray([
                    'name' => 'add-order-value-discount',
                    'class' => AddOrderValueDiscount::class,
                    'params' => [
                        'discount' => [
                            'required' => true,
                            'type' => 'float',
                        ],
                    ],
                ]),
                new RevenueHigherThan(['amount' => 1200]),
                new AddOrderValueDiscount(['discount' => 5]),
                DiscountClashResolvementEnum::UseHighestValue,
                [],
            ],
            [
                $this->getTestOrderWithCustomer(1),
                RuleEntity::fromArray([
                    'id' => 123,
                    'title' => 'add-0-euro-when-revenue-higher-than-200',
                    'condition' => 'customer-revenue-higher-than',
                    'condition-params' => [
                        'amount' => 200
                    ],
                    'action' => 'add-order-value-discount',
                    'action-params' => [
                        'discount' => 0
                    ]
                ]),
                ConditionEntity::fromArray([
                    'name' => 'customer-revenue-higher-than',
                    'class' => RevenueHigherThan::class,
                    'params' => [
                        'amount' => [
                            'required' => true,
                            'type' => 'float',
                        ],
                    ],
                ]),
                ActionEntity::fromArray([
                    'name' => 'add-order-value-discount',
                    'class' => AddOrderValueDiscount::class,
                    'params' => [
                        'discount' => [
                            'required' => true,
                            'type' => 'float',
                        ],
                    ],
                ]),
                new RevenueHigherThan(['amount' => 200]),
                new AddOrderValueDiscount(['discount' => 0]),
                DiscountClashResolvementEnum::UseHighestValue,
                [],
            ],
            [
                $this->getTestOrderWithCustomer(1),
                RuleEntity::fromArray([
                    'id' => 123,
                    'title' => 'add-free-item-when-revenue-higher-than-200',
                    'condition' => 'customer-revenue-higher-than',
                    'condition-params' => [
                        'amount' => 200
                    ],
                    'action' => 'add-free-item',
                    'action-params' => [
                        'productId' => 'A101',
                        'quantity' => '2',
                    ]
                ]),
                ConditionEntity::fromArray([
                    'name' => 'customer-revenue-higher-than',
                    'class' => RevenueHigherThan::class,
                    'params' => [
                        'amount' => [
                            'required' => true,
                            'type' => 'float',
                        ],
                    ],
                ]),
                ActionEntity::fromArray([
                    'name' => 'add-free-item',
                    'class' => AddFreeItems::class,
                    'params' => [
                        'productId' => 'A101',
                        'quantity' => 2,
                    ],
                ]),
                new RevenueHigherThan(['amount' => 200]),
                new AddFreeItems(['productId' => 'A101', 'quantity' => 2]),
                DiscountClashResolvementEnum::UseHighestValue,
                [
                    'free-items' => [
                        [
                            'product-id' => 'A101',
                            'quantity' => 2,
                            'reason' => 'add-free-item-when-revenue-higher-than-200',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider applyRulesDataProvider
     * @param OrderWithContextAggregate $order
     * @param RuleEntity $ruleEntity
     * @param ConditionEntity $conditionEntity
     * @param ActionEntity $actionEntity
     * @param ConditionInterface $condition
     * @param ActionInterface $action
     * @param DiscountClashResolvementEnum $clashStrategy
     * @param array $expectedDiscounts
     * @return void
     */
    public function testCalculateDiscounts(
        OrderWithContextAggregate $order,
        RuleEntity $ruleEntity,
        ConditionEntity $conditionEntity,
        ActionEntity $actionEntity,
        ConditionInterface $condition,
        ActionInterface $action,
        DiscountClashResolvementEnum $clashStrategy,
        array $expectedDiscounts
    ): void {

        $aggregate = RulesAggregate::fromArray([
            'rule' => $ruleEntity,
            'condition-entity' => $conditionEntity,
            'action-entity' => $actionEntity,
            'condition' => $condition,
            'action' => $action,
        ]);
        $discounts = new DiscountAggregate($clashStrategy, $clashStrategy, $clashStrategy);

        $aggregate->calculateDiscount($discounts, $order);

        $this->assertEquals($expectedDiscounts, $discounts->toArray());
    }


    /**
     * Test array creating + getters
     *
     * @return void
     */
    public function testCreateFromArray(): void
    {
        $rule = new RuleEntity('a', 'b', [], 'c', [], 1);
        $conditionEntity = new ConditionEntity('x', 'y', []);
        $actionEntity = new ActionEntity('j', 'k', []);
        $condition = new RevenueHigherThan([]);
        $action = new AddFreeItems([]);

        $aggregate = RulesAggregate::fromArray([
            'rule' => $rule,
            'condition-entity' => $conditionEntity,
            'action-entity' => $actionEntity,
            'condition' => $condition,
            'action' => $action,
        ]);

        $this->assertEquals($rule, $aggregate->getRule());
        $this->assertEquals($conditionEntity, $aggregate->getConditionEntity());
        $this->assertEquals($actionEntity, $aggregate->getActionEntity());
        $this->assertEquals($condition, $aggregate->getCondition());
        $this->assertEquals($action, $aggregate->getAction());
    }
}