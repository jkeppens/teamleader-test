<?php

namespace JkeppensTest\Discounts\Unit\Domain\Actions;

use Jkeppens\Discounts\Domain\Actions\AddOrderItemPercentageDiscount;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntityCollection;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;
use RuntimeException;

class AddOrderItemPercentageDiscountTest extends AbstractTestWithOrderData
{
    public function paramsDataProvider(): array
    {
        return [
            [2, 0, ['percentage' => 0], null, null, null, null],
            [2, 0, ['percentage' => 10], OrderItemDiscountEntity::class, 10, 2.5, 'B102'],
            [2, 0, ['percentage' => 20], OrderItemDiscountEntity::class, 20, 4.99, 'B102'],
        ];
    }

    /**
     * @dataProvider paramsDataProvider
     *
     * @param int $orderId
     * @param int $itemIndex
     * @param array $params
     * @param string|null $expectedClass
     * @param int|null $expectedPercentage
     * @param float|null $expectedDiscount
     * @param string|null $expectedProductId
     * @return void
     */
    public function testAddOrderItemPercentageDiscountUsingParams(
        int $orderId,
        int $itemIndex,
        array $params,
        string|null $expectedClass,
        int|null $expectedPercentage,
        float|null $expectedDiscount,
        string|null $expectedProductId
    ): void {
        $order = $this->getTestOrderWithCustomer($orderId);

        $action = new AddOrderItemPercentageDiscount($params);
        $items = $order->getOrder()->getItems();
        $discount = $action->apply($items[$itemIndex]);

        if ($expectedClass === null) {
            $this->assertNull($discount);
        } else {
            $this->assertInstanceOf($expectedClass, $discount);

            /** @var OrderItemDiscountEntity $discount */
            $this->assertEquals($expectedProductId, $discount->getProductId());
            $this->assertEquals($expectedPercentage, $discount->getPercentage());
            $this->assertEquals($expectedDiscount, $discount->getDiscount());
        }
    }

    /**
     * @return void
     */
    public function testSendWrongTypeOfData(): void
    {
        $this->expectException(RuntimeException::class);

        $action = new AddOrderItemPercentageDiscount(['percentage' => 10]);
        $action->apply(new RuleEntityCollection());
    }
}
