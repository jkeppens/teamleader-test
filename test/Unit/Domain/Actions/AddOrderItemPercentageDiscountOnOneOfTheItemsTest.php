<?php

namespace JkeppensTest\Discounts\Unit\Domain\Actions;

use Jkeppens\Discounts\Domain\Actions\AddOrderItemPercentageDiscountOnOneOfTheItems;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderItemDiscountEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntityCollection;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;
use LogicException;
use RuntimeException;

class AddOrderItemPercentageDiscountOnOneOfTheItemsTest extends AbstractTestWithOrderData
{
    public function paramsDataProvider(): array
    {
        return [
            [3, ['percentage' => 0, 'checkOn' => 'xxxx', 'checkFor' => 'min'], null, null, null, null, LogicException::class],
            [3, ['percentage' => 0, 'checkOn' => 'unit_price', 'checkFor' => 'xxxxx'], null, null, null, null, LogicException::class],
            [3, ['percentage' => 0, 'checkOn' => 'unit_price', 'checkFor' => 'min'], null, null, null, null, null],
            [3, ['percentage' => 10, 'checkOn' => 'unit_price', 'checkFor' => 'min'], OrderItemDiscountEntity::class, 10, 1.95, "A101", null],
            [3, ['percentage' => 10, 'checkOn' => 'unit_price', 'checkFor' => 'max'], OrderItemDiscountEntity::class, 10, 4.95, "A102", null],
            [3, ['percentage' => 10, 'checkOn' => 'quantity', 'checkFor' => 'min'], OrderItemDiscountEntity::class, 10, 4.95, "A102", null],
            [3, ['percentage' => 10, 'checkOn' => 'quantity', 'checkFor' => 'max'], OrderItemDiscountEntity::class, 10, 1.95, "A101", null],
            [3, ['percentage' => 10, 'checkOn' => 'total', 'checkFor' => 'min'], OrderItemDiscountEntity::class, 10, 1.95, "A101", null],
            [3, ['percentage' => 10, 'checkOn' => 'total', 'checkFor' => 'max'], OrderItemDiscountEntity::class, 10, 4.95, "A102", null],
        ];
    }

    /**
     * @dataProvider paramsDataProvider
     *
     * @param int $orderId
     * @param array $params
     * @param string|null $expectedClass
     * @param int|null $expectedPercentage
     * @param float|null $expectedDiscount
     * @param string|null $expectedProductId
     * @param string|null $expectedException
     * @return void
     */
    public function testAddOrderItemPercentageDiscountOnOneOfTheItemsUsingParams(
        int $orderId,
        array $params,
        string|null $expectedClass,
        int|null $expectedPercentage,
        float|null $expectedDiscount,
        string|null $expectedProductId,
        string|null $expectedException
    ): void {
        $order = $this->getTestOrderWithCustomer($orderId);

        if ($expectedException) {
            $this->expectException($expectedException);
        }

        $action = new AddOrderItemPercentageDiscountOnOneOfTheItems($params);
        $items = $order->getOrder()->getItems();
        $discount = $action->apply($items);

        if ($expectedClass === null) {
            $this->assertNull($discount);
        } else {
            $this->assertInstanceOf($expectedClass, $discount);

            /** @var OrderItemDiscountEntity $discount */
            $this->assertEquals($expectedProductId, $discount->getProductId());
            $this->assertEquals($expectedPercentage, $discount->getPercentage());
            $this->assertEquals($expectedDiscount, $discount->getDiscount());
        }
    }

    /**
     * @return void
     */
    public function testSendWrongTypeOfData(): void
    {
        $this->expectException(RuntimeException::class);

        $action = new AddOrderItemPercentageDiscountOnOneOfTheItems(
            ['percentage' => 10, 'checkOn' => 'unit_price', 'checkFor' => 'min']
        );
        $action->apply(new RuleEntityCollection());
    }
}
