<?php

namespace JkeppensTest\Discounts\Unit\Domain\Actions;

use Jkeppens\Discounts\Domain\Actions\AddOrderValueDiscount;
use Jkeppens\Discounts\Domain\Entity\Discount\OrderDiscountEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntityCollection;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;
use RuntimeException;

class AddOrderValueDiscountTest extends AbstractTestWithOrderData
{
    public function paramsDataProvider(): array
    {
        return [
            [2, ['discount' => 0], null, null, null],
            [2, ['discount' => 10], OrderDiscountEntity::class, null, 10],
            [2, ['discount' => 20], OrderDiscountEntity::class, null, 20],
        ];
    }

    /**
     * @dataProvider paramsDataProvider
     *
     * @param int $orderId
     * @param array $params
     * @param string|null $expectedClass
     * @param int|null $expectedPercentage
     * @param float|null $expectedDiscount
     * @return void
     */
    public function testAddOrderItemPercentageDiscountUsingParams(
        int $orderId,
        array $params,
        string|null $expectedClass,
        int|null $expectedPercentage,
        float|null $expectedDiscount,
    ): void {
        $order = $this->getTestOrderWithCustomer($orderId);

        $action = new AddOrderValueDiscount($params);
        $discount = $action->apply($order);

        if ($expectedClass === null) {
            $this->assertNull($discount);
        } else {
            $this->assertInstanceOf($expectedClass, $discount);

            /** @var OrderDiscountEntity $discount */
            $this->assertEquals($expectedPercentage, $discount->getPercentage());
            $this->assertEquals($expectedDiscount, $discount->getDiscount());
        }
    }

    /**
     * @return void
     */
    public function testSendWrongTypeOfData(): void
    {
        $this->expectException(RuntimeException::class);

        $action = new AddOrderValueDiscount(['discount' => 1]);
        $action->apply(new RuleEntityCollection());
    }
}
