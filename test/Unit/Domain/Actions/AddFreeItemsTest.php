<?php

namespace JkeppensTest\Discounts\Unit\Domain\Actions;

use Jkeppens\Discounts\Domain\Actions\AddFreeItems;
use Jkeppens\Discounts\Domain\Entity\Discount\FreeItemEntity;
use Jkeppens\Discounts\Domain\Entity\RuleEntityCollection;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;
use RuntimeException;

class AddFreeItemsTest extends AbstractTestWithOrderData
{
    public function paramsDataProvider(): array
    {
        return [
            [2, 0, ['quantity' => 1, 'quantityPerChunkOf' => 5, 'productId' => 'A101'], FreeItemEntity::class, 1, 'A101'],
            [2, 0, ['quantity' => 1, 'quantityPerChunkOf' => 15], null, null, null],
            [2, 0, ['quantity' => 5, 'quantityPerChunkOf' => 2], FreeItemEntity::class, 10, 'B102'],
            [2, 0, ['quantity' => 3, 'productId' => 'A101'], FreeItemEntity::class, 3, 'A101'],
            [2, 0, ['quantity' => 5], FreeItemEntity::class, 5, 'B102'],
        ];
    }

    /**
     * @dataProvider paramsDataProvider
     *
     * @param int $orderId
     * @param int $itemIndex
     * @param array $params
     * @param string|null $expectedClass
     * @param int|null $expectedQuantity
     * @param string|null $expectedProductId
     * @return void
     */
    public function testFreeAddItemsActionUsingParams(
        int $orderId,
        int $itemIndex,
        array $params,
        string|null $expectedClass,
        int|null $expectedQuantity,
        string|null $expectedProductId
    ): void {
        $order = $this->getTestOrderWithCustomer($orderId);

        $action = new AddFreeItems($params);
        $items = $order->getOrder()->getItems();
        $discount = $action->apply($items[$itemIndex]);

        if ($expectedClass === null) {
            $this->assertNull($discount);
        } else {
            $this->assertInstanceOf($expectedClass, $discount);

            /** @var FreeItemEntity $discount */
            $this->assertEquals($expectedProductId, $discount->getProductId());
            $this->assertEquals($expectedQuantity, $discount->getQuantity());
        }
    }

    /**
     * @return void
     */
    public function testSendWrongTypeOfData(): void
    {
        $this->expectException(RuntimeException::class);

        $action = new AddFreeItems(
            ['quantity' => 1, 'quantityPerChunkOf' => 5, 'productId' => 'A101']
        );
        $action->apply(new RuleEntityCollection());
    }

    /**
     * @return void
     */
    public function testSendingOrderRequiresAProductId(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Product ID required when passing an order');

        $action = new AddFreeItems(
            ['quantity' => 1]
        );
        $action->apply($this->getTestOrderWithCustomer(1));
    }

    /**
     * @return void
     */
    public function testSendingOrderCannotHaveChunkConfig(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('QuantityPerChunkOf not possible when passing an order');

        $action = new AddFreeItems(
            ['quantity' => 1, 'quantityPerChunkOf' => 5, 'productId' => 'A101']
        );
        $action->apply($this->getTestOrderWithCustomer(1));
    }
}
