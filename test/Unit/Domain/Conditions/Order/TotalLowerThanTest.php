<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\Order;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\Order\OrderTotalLowerThan;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class TotalLowerThanTest extends AbstractTestWithOrderData
{
    public function amountValuesDataProvider(): array
    {
        return [
            [1, ['amount' => 70], true],
            [1, ['amount' => 40], false],
            [1, [], true],
        ];
    }

    /**
     * @dataProvider amountValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testCustomerRevenueLowerThan(
        int $orderId,
        array $params,
        bool $expectsResult,
    ): void {
        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new OrderTotalLowerThan($params);
        $appliesTo = $condition->appliesTo($order);

        if ($expectsResult) {
            $this->assertInstanceOf(OrderWithContextAggregate::class, $appliesTo);
        } else {
            $this->assertNull($appliesTo);
        }
    }
}