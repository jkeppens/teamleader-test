<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\Order;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\Order\OrderTotalBetween;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class TotalBetweenTest extends AbstractTestWithOrderData
{
    public function amountValuesDataProvider(): array
    {
        return [
            [1, ['amountFrom' => 30, 'amountUntil' => 70], true],
            [1, ['amountFrom' => 70, 'amountUntil' => 100], false],
            [1, ['amountFrom' => 30], true],
            [1, ['amountUntil' => 70], true],
            [1, ['amountFrom' => 70], false],
            [1, ['amountUntil' => 30], false],
            [1, [], true],
        ];
    }

    /**
     * @dataProvider amountValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testOrderTotalInDefinedRange(
        int $orderId,
        array $params,
        bool $expectsResult,
    ): void {
        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new OrderTotalBetween($params);
        $appliesTo = $condition->appliesTo($order);

        if ($expectsResult) {
            $this->assertInstanceOf(OrderWithContextAggregate::class, $appliesTo);
        } else {
            $this->assertNull($appliesTo);
        }
    }
}