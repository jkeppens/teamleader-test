<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\OrderItems;

use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForEveryProductInCategoryWithQuantityOfItemsLowerThan;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class ForEveryProductInCategoryWithQuantityOfItemsLowerThanTest extends AbstractTestWithOrderData
{
    /**
     * @return array[]
     */
    public function quantityValuesDataProvider(): array
    {
        return [
            [2, ['categoryId' => 2, 'quantity' => 7], true],
            [2, ['categoryId' => 2, 'quantity' => 3], false],
            [2, ['categoryId' => 2], true],
            [2, ['categoryId' => 1], false],
        ];
    }

    /**
     * @dataProvider quantityValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testOrderItemsSelectedLowerThanProvidedQuantity(
        int $orderId,
        array $params,
        bool $expectsResult
    ): void {

        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new ForEveryProductInCategoryWithQuantityOfItemsLowerThan($params);
        $appliesTo = $condition->appliesTo($order);

        $this->assertIsArray($appliesTo);
        if ($expectsResult) {
            $this->assertEquals(1, count($appliesTo));
        } else {
            $this->assertEquals(0, count($appliesTo));
        }
    }
}