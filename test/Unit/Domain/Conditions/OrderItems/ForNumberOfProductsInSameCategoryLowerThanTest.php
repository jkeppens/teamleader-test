<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\OrderItems;

use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForNumberOfProductsInSameCategoryLowerThan;
use Jkeppens\Ordering\Domain\Aggregate\OrderItemAggregateCollection;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class ForNumberOfProductsInSameCategoryLowerThanTest extends AbstractTestWithOrderData
{
    /**
     * @return array[]
     */
    public function quantityValuesDataProvider(): array
    {
        return [
            [3, ['categoryId' => 1, 'quantity' => 5], true],
            [3, ['categoryId' => 1, 'quantity' => 1], false],
            [3, ['categoryId' => 1], true],
            [3, ['categoryId' => 2], false],
        ];
    }

    /**
     * @dataProvider quantityValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testOrderItemsSelectedLowerThanSpecifiedNumberOfProductsInCategory(
        int $orderId,
        array $params,
        bool $expectsResult
    ): void {

        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new ForNumberOfProductsInSameCategoryLowerThan($params);
        $appliesTo = $condition->appliesTo($order);

        $this->assertIsArray($appliesTo);
        if ($expectsResult) {
            $this->assertEquals(1, count($appliesTo));
            $this->assertInstanceOf(OrderItemAggregateCollection::class, reset($appliesTo));
        } else {
            $this->assertEquals(0, count($appliesTo));
        }
    }
}
