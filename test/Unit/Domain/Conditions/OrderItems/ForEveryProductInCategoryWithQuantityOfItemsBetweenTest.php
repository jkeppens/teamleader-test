<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\OrderItems;

use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForEveryProductInCategoryWithQuantityOfItemsBetween;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class ForEveryProductInCategoryWithQuantityOfItemsBetweenTest extends AbstractTestWithOrderData
{
    /**
     * @return array[]
     */
    public function quantityValuesDataProvider(): array
    {
        return [
            [2, ['categoryId' => 2, 'quantityFrom' => 3, 'quantityUntil' => 7], true],
            [2, ['categoryId' => 2, 'quantityFrom' => 7, 'quantityUntil' => 10], false],
            [2, ['categoryId' => 2, 'quantityFrom' => 3], true],
            [2, ['categoryId' => 2, 'quantityUntil' => 7], true],
            [2, ['categoryId' => 2, 'quantityFrom' => 7], false],
            [2, ['categoryId' => 2, 'quantityUntil' => 3], false],
            [2, ['categoryId' => 2], true],
            [2, ['categoryId' => 1], false],
        ];
    }

    /**
     * @dataProvider quantityValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testOrderItemsSelectedBasedOnProvidedQuantityRange(
        int $orderId,
        array $params,
        bool $expectsResult
    ): void {

        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new ForEveryProductInCategoryWithQuantityOfItemsBetween($params);
        $appliesTo = $condition->appliesTo($order);

        $this->assertIsArray($appliesTo);
        if ($expectsResult) {
            $this->assertEquals(1, count($appliesTo));
        } else {
            $this->assertEquals(0, count($appliesTo));
        }
    }
}