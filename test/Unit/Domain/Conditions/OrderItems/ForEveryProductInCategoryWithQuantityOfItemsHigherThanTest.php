<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\OrderItems;

use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForEveryProductInCategoryWithQuantityOfItemsHigherThan;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class ForEveryProductInCategoryWithQuantityOfItemsHigherThanTest extends AbstractTestWithOrderData
{
    /**
     * @return array[]
     */
    public function quantityValuesDataProvider(): array
    {
        return [
            [2, ['categoryId' => 2, 'quantity' => 3], true],
            [2, ['categoryId' => 2, 'quantity' => 7], false],
            [2, ['categoryId' => 2], true],
            [2, ['categoryId' => 1], false],
        ];
    }

    /**
     * @dataProvider quantityValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testOrderItemsSelectedHigherThanProvidedQuantit(
        int $orderId,
        array $params,
        bool $expectsResult
    ): void {

        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new ForEveryProductInCategoryWithQuantityOfItemsHigherThan($params);
        $appliesTo = $condition->appliesTo($order);

        $this->assertIsArray($appliesTo);
        if ($expectsResult) {
            $this->assertEquals(1, count($appliesTo));
        } else {
            $this->assertEquals(0, count($appliesTo));
        }
    }
}