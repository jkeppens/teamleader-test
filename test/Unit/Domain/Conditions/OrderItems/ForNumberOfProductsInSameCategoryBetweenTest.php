<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\OrderItems;

use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForNumberOfProductsInSameCategoryBetween;
use Jkeppens\Ordering\Domain\Aggregate\OrderItemAggregateCollection;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class ForNumberOfProductsInSameCategoryBetweenTest extends AbstractTestWithOrderData
{
    /**
     * @return array[]
     */
    public function quantityValuesDataProvider(): array
    {
        return [
            [3, ['categoryId' => 1, 'quantityFrom' => 1, 'quantityUntil' => 3], true],
            [3, ['categoryId' => 1, 'quantityFrom' => 5, 'quantityUntil' => 10], false],
            [3, ['categoryId' => 1, 'quantityFrom' => 1], true],
            [3, ['categoryId' => 1, 'quantityUntil' => 3], true],
            [3, ['categoryId' => 1, 'quantityFrom' => 7], false],
            [3, ['categoryId' => 1, 'quantityUntil' => 1], false],
            [3, ['categoryId' => 1], true],
            [3, ['categoryId' => 2], false],
        ];
    }

    /**
     * @dataProvider quantityValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testOrderItemsSelectedBasedOnNumberRangeOfProductsInCategory(
        int $orderId,
        array $params,
        bool $expectsResult
    ): void {

        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new ForNumberOfProductsInSameCategoryBetween($params);
        $appliesTo = $condition->appliesTo($order);

        $this->assertIsArray($appliesTo);
        if ($expectsResult) {
            $this->assertEquals(1, count($appliesTo));
            $this->assertInstanceOf(OrderItemAggregateCollection::class, reset($appliesTo));
        } else {
            $this->assertEquals(0, count($appliesTo));
        }
    }
}
