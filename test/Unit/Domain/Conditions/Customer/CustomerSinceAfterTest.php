<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\Customer;

use Carbon\Carbon;
use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\Customer\CustomerSinceAfter;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class CustomerSinceAfterTest extends AbstractTestWithOrderData
{
    public function dateValuesDataProvider(): array
    {
        return [
            [1, ['date' => new Carbon('2020-01-01')], true],
            [1, ['date' => new Carbon('2021-01-01')], false],
            [1, [], true],
        ];
    }

    /**
     * @dataProvider dateValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testCustomerJoinedAfterProvidedDate(
        int $orderId,
        array $params,
        bool $expectsResult,
    ): void {
        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new CustomerSinceAfter($params);
        $appliesTo = $condition->appliesTo($order);

        if ($expectsResult) {
            $this->assertInstanceOf(OrderWithContextAggregate::class, $appliesTo);
        } else {
            $this->assertNull($appliesTo);
        }
    }
}