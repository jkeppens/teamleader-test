<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\Customer;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\Customer\RevenueHigherThan;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class RevenueHigherThanTest extends AbstractTestWithOrderData
{
    public function amountValuesDataProvider(): array
    {
        return [
            [1, ['amount' => 400], true],
            [1, ['amount' => 700], false],
            [1, [], true],
        ];
    }

    /**
     * @dataProvider amountValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testRevenueHigherThanSuccess(
        int $orderId,
        array $params,
        bool $expectsResult,
    ): void
    {
        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new RevenueHigherThan($params);
        $appliesTo = $condition->appliesTo($order);

        if ($expectsResult) {
            $this->assertInstanceOf(OrderWithContextAggregate::class, $appliesTo);
        } else {
            $this->assertNull($appliesTo);
        }
    }
}