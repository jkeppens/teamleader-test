<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\Customer;

use Carbon\Carbon;
use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\Customer\CustomerSinceBetween;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class CustomerSinceBetweenTest extends AbstractTestWithOrderData
{
    public function dateValuesDataProvider(): array
    {
        $date20190101 = new Carbon('2019-01-01');
        $date20200101 = new Carbon('2020-01-01');
        $date20210101 = new Carbon('2021-01-01');
        $date20220101 = new Carbon('2022-01-01');
        return [
            [1, ['dateFrom' => $date20200101, 'dateUntil' => $date20210101], true],
            [1, ['dateFrom' => $date20210101, 'dateUntil' => $date20220101], false],
            [1, ['dateFrom' => $date20190101, 'dateUntil' => $date20200101], false],
            [1, ['dateFrom' => $date20200101], true],
            [1, ['dateUntil' => $date20220101], true],
            [1, ['dateFrom' => $date20220101], false],
            [1, ['dateUntil' => $date20200101], false],
            [1, [], true],
        ];
    }

    /**
     * @dataProvider dateValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testCustomerJoinedBetweenTwoProvidedDates(
        int $orderId,
        array $params,
        bool $expectsResult,
    ): void
    {
        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new CustomerSinceBetween($params);
        $appliesTo = $condition->appliesTo($order);

        if ($expectsResult) {
            $this->assertInstanceOf(OrderWithContextAggregate::class, $appliesTo);
        } else {
            $this->assertNull($appliesTo);
        }
    }
}
