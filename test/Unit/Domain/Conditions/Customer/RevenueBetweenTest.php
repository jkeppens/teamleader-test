<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\Customer;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\Customer\RevenueBetween;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class RevenueBetweenTest extends AbstractTestWithOrderData
{
    public function amountValuesDataProvider(): array
    {
        return [
            [1, ['amountFrom' => 400, 'amountUntil' => 500], true],
            [1, ['amountFrom' => 500, 'amountUntil' => 600], false],
            [1, ['amountFrom' => 400], true],
            [1, ['amountUntil' => 500], true],
            [1, ['amountFrom' => 700], false],
            [1, ['amountUntil' => 400], false],
            [1, [], true],
        ];
    }

    /**
     * @dataProvider amountValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testCustomerRevenueInDefinedRange(
        int $orderId,
        array $params,
        bool $expectsResult,
    ): void
    {
        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new RevenueBetween($params);
        $appliesTo = $condition->appliesTo($order);

        if ($expectsResult) {
            $this->assertInstanceOf(OrderWithContextAggregate::class, $appliesTo);
        } else {
            $this->assertNull($appliesTo);
        }
    }
}