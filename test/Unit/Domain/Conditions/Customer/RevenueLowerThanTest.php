<?php

namespace JkeppensTest\Discounts\Unit\Domain\Conditions\Customer;

use Jkeppens\Discounts\Domain\Aggregate\OrderWithContextAggregate;
use Jkeppens\Discounts\Domain\Conditions\Customer\RevenueLowerThan;
use JkeppensTest\Discounts\Unit\Domain\AbstractTestWithOrderData;

class RevenueLowerThanTest extends AbstractTestWithOrderData
{
    public function amountValuesDataProvider(): array
    {
        return [
            [1, ['amount' => 500], true],
            [1, ['amount' => 300], false],
            [1, [], true],
        ];
    }

    /**
     * @dataProvider amountValuesDataProvider
     * @param int $orderId
     * @param array $params
     * @param bool $expectsResult
     * @return void
     */
    public function testRevenueLowerThanSuccess(
        int $orderId,
        array $params,
        bool $expectsResult,
    ): void
    {
        $order = $this->getTestOrderWithCustomer($orderId);

        $condition = new RevenueLowerThan($params);
        $appliesTo = $condition->appliesTo($order);

        if ($expectsResult) {
            $this->assertInstanceOf(OrderWithContextAggregate::class, $appliesTo);
        } else {
            $this->assertNull($appliesTo);
        }
    }
}