<?php

declare(strict_types=1);

use Jkeppens\Discounts\UI\Http\Handler\CalculateDiscountForOrderHandler;
use Jkeppens\Discounts\UI\Http\Handler\CalculateDiscountForOrderIdHandler;
use Jkeppens\Discounts\UI\Http\Handler\Action\ListActionsHandler;
use Jkeppens\Discounts\UI\Http\Handler\Condition\ListConditionsHandler;
use Jkeppens\Discounts\UI\Http\Handler\Rule\CreateRuleHandler;
use Jkeppens\Discounts\UI\Http\Handler\Rule\DeleteRuleHandler;
use Jkeppens\Discounts\UI\Http\Handler\Rule\ListRulesHandler;
use Jkeppens\Discounts\UI\Http\Handler\Rule\ModifyRuleHandler;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\ServerRequest;
use Mezzio\Application;
use Mezzio\MiddlewareFactory;
use Psr\Container\ContainerInterface;

return static function (Application $app, MiddlewareFactory $factory, ContainerInterface $container): void {
    $app->get('/', function (ServerRequest $request) {
        return new HtmlResponse('Hello World');
    });

    $app->post('/api/calculate-discount', CalculateDiscountForOrderHandler::class);
    $app->post('/api/calculate-discount-for-order-id', CalculateDiscountForOrderIdHandler::class);

    $app->get('/api/discounts/rules', ListRulesHandler::class, 'discounts.rules.list');
    $app->post('/api/discounts/rules', CreateRuleHandler::class, 'discounts.rules.create');
    $app->put('/api/discounts/rules/:rule-id', ModifyRuleHandler::class, 'discounts.rules.modify');
    $app->delete('/api/discounts/rules/:rule-id', DeleteRuleHandler::class, 'discounts.rules.delete');

    $app->get('/api/discounts/conditions', ListConditionsHandler::class, 'discounts.conditions.list');
    $app->get('/api/discounts/actions', ListActionsHandler::class, 'discounts.actions.list');

};
