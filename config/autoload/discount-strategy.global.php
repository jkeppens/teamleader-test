<?php

use Jkeppens\Discounts\Domain\Enum\DiscountClashResolvementEnum;

return [
    'discount-strategy' => [
        'order' => DiscountClashResolvementEnum::UseHighestValue,
        'order-items' => DiscountClashResolvementEnum::UseHighestValue,
        'free-items' => DiscountClashResolvementEnum::AddDiscount,
    ],
];
