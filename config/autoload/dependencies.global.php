<?php

use Jkeppens\Catalog\Domain\Repository\ProductRepository;
use Jkeppens\Catalog\Infrastructure\File\ProductFileRepository;
use Jkeppens\Customer\Domain\Repository\CustomerRepository;
use Jkeppens\Customer\Infrastructure\File\CustomerFileRepository;
use Jkeppens\Discounts\Domain\Repository\ActionRepository;
use Jkeppens\Discounts\Domain\Repository\ConditionRepository;
use Jkeppens\Discounts\Domain\Repository\RuleRepository;
use Jkeppens\Discounts\Infrastructure\File\Json\RuleFileRepository;
use Jkeppens\Discounts\Infrastructure\File\Php\ActionFileRepository;
use Jkeppens\Discounts\Infrastructure\File\Php\ConditionFileRepository;
use Jkeppens\Ordering\Domain\Repository\OrderRepository;
use Jkeppens\Ordering\Infrastructure\File\OrderFileRepository;
use League\Tactician\CommandBus;
use League\Tactician\Handler\Mapping\MapByNamingConvention\ClassName\Suffix as SuffixClassNameInflector;
use League\Tactician\Handler\Mapping\MapByNamingConvention\MapByNamingConvention;
use League\Tactician\Handler\Mapping\MapByNamingConvention\MethodName\Invoke as InvokeMethodInflector;
use Psr\Container\ContainerInterface;
use League\Tactician\Handler\CommandHandlerMiddleware;

return [
    'dependencies' => [
        'aliases' => [
            CustomerRepository::class => CustomerFileRepository::class,
            OrderRepository::class => OrderFileRepository::class,
            ProductRepository::class => ProductFileRepository::class,
            ActionRepository::class => ActionFileRepository::class,
            ConditionRepository::class => ConditionFileRepository::class,
            RuleRepository::class => RuleFileRepository::class,
        ],
        'invokables' => [
        ],
        'factories' => [
            CommandHandlerMiddleware::class => function (ContainerInterface $container) {
                $methodInflector = new InvokeMethodInflector();
                $classNameInflector = new SuffixClassNameInflector('Handler');
                return new CommandHandlerMiddleware(
                    $container,
                    new MapByNamingConvention($classNameInflector, $methodInflector)
                );
            },
            CommandBus::class => function (ContainerInterface $container) {
                return new CommandBus(
                    $container->get(CommandHandlerMiddleware::class),
                );
            },
        ],
    ],
];
