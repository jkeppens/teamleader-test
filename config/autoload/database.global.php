<?php

return [
    'database' => [
        'json' => [
            'files' => [
                'products' => __DIR__ . '/../../data/json/database/products.json',
                'customers' => __DIR__ . '/../../data/json/database/customers.json',
                'rules' => __DIR__ . '/../../data/json/database/rules.json',
            ],
            'paths' => [
                'orders' => __DIR__ . '/../../data/json/database/orders',
            ],
        ],
        'php' => [
            'files' => [
                'conditions' => __DIR__ . '/../../data/php/database/conditions.php',
                'actions' => __DIR__ . '/../../data/php/database/actions.php',
            ],
        ],
    ],
];
