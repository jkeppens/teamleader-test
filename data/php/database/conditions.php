<?php

use Jkeppens\Discounts\Domain\Conditions\Customer\CustomerSinceAfter;
use Jkeppens\Discounts\Domain\Conditions\Customer\CustomerSinceBefore;
use Jkeppens\Discounts\Domain\Conditions\Customer\RevenueBetween;
use Jkeppens\Discounts\Domain\Conditions\Customer\RevenueHigherThan;
use Jkeppens\Discounts\Domain\Conditions\Customer\RevenueLowerThan;
use Jkeppens\Discounts\Domain\Conditions\Order\OrderTotalBetween;
use Jkeppens\Discounts\Domain\Conditions\Order\OrderTotalHigherThan;
use Jkeppens\Discounts\Domain\Conditions\Order\OrderTotalLowerThan;
use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForEveryProductInCategoryWithQuantityOfItemsBetween;
use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForEveryProductInCategoryWithQuantityOfItemsExactly;
use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForEveryProductInCategoryWithQuantityOfItemsHigherThan;
use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForEveryProductInCategoryWithQuantityOfItemsLowerThan;
use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForNumberOfProductsInSameCategoryBetween;
use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForNumberOfProductsInSameCategoryExactly;
use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForNumberOfProductsInSameCategoryHigherThan;
use Jkeppens\Discounts\Domain\Conditions\OrderItems\ForNumberOfProductsInSameCategoryLowerThan;

return [
    [
        'name' => 'customer-revenue-between',
        'class' => RevenueBetween::class,
        'params' => [
            'amountFrom' => [
                'required' => false,
                'type' => 'float',
            ],
            'amountUntil' => [
                'required' => false,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'customer-revenue-higher-than',
        'class' => RevenueHigherThan::class,
        'params' => [
            'amount' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'customer-revenue-lower-than',
        'class' => RevenueLowerThan::class,
        'params' => [
            'amount' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'customer-since-between',
        'class' => RevenueBetween::class,
        'params' => [
            'dateFrom' => [
                'required' => false,
                'type' => 'date',
            ],
            'dateUntil' => [
                'required' => false,
                'type' => 'date',
            ],
        ],
    ],
    [
        'name' => 'customer-since-after',
        'class' => CustomerSinceAfter::class,
        'params' => [
            'date' => [
                'required' => true,
                'type' => 'date',
            ],
        ],
    ],
    [
        'name' => 'customer-since-before',
        'class' => CustomerSinceBefore::class,
        'params' => [
            'date' => [
                'required' => true,
                'type' => 'date',
            ],
        ],
    ],
    [
        'name' => 'for-every-product-in-category-with-quantity-of-items-between',
        'class' => ForEveryProductInCategoryWithQuantityOfItemsBetween::class,
        'params' => [
            'quantityFrom' => [
                'required' => false,
                'type' => 'int',
            ],
            'quantityUntil' => [
                'required' => false,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'for-every-product-in-category-with-quantity-of-items-exactly',
        'class' => ForEveryProductInCategoryWithQuantityOfItemsExactly::class,
        'params' => [
            'quantity' => [
                'required' => true,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'for-every-product-in-category-with-quantity-of-items-higher-than',
        'class' => ForEveryProductInCategoryWithQuantityOfItemsHigherThan::class,
        'params' => [
            'quantity' => [
                'required' => true,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'for-every-product-in-category-with-quantity-of-items-lower-than',
        'class' => ForEveryProductInCategoryWithQuantityOfItemsLowerThan::class,
        'params' => [
            'quantity' => [
                'required' => true,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'for-number-of-products-in-same-category-between',
        'class' => ForNumberOfProductsInSameCategoryBetween::class,
        'params' => [
            'quantityFrom' => [
                'required' => false,
                'type' => 'int',
            ],
            'quantityUntil' => [
                'required' => false,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'for-number-of-products-in-same-category-exactly',
        'class' => ForNumberOfProductsInSameCategoryExactly::class,
        'params' => [
            'quantity' => [
                'required' => true,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'for-number-of-products-in-same-category-higher-than',
        'class' => ForNumberOfProductsInSameCategoryHigherThan::class,
        'params' => [
            'quantity' => [
                'required' => true,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'for-number-of-products-in-same-category-lower-than',
        'class' => ForNumberOfProductsInSameCategoryLowerThan::class,
        'params' => [
            'quantity' => [
                'required' => true,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'order-total-between',
        'class' => OrderTotalBetween::class,
        'params' => [
            'amountFrom' => [
                'required' => false,
                'type' => 'float',
            ],
            'amountUntil' => [
                'required' => false,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'order-total-higher-than',
        'class' => OrderTotalHigherThan::class,
        'params' => [
            'amount' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'order-total-lower-than',
        'class' => OrderTotalLowerThan::class,
        'params' => [
            'amount' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ],
];