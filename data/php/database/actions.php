<?php

use Jkeppens\Discounts\Domain\Actions\AddFreeItems;
use Jkeppens\Discounts\Domain\Actions\AddOrderItemPercentageDiscount;
use Jkeppens\Discounts\Domain\Actions\AddOrderItemPercentageDiscountOnOneOfTheItems;
use Jkeppens\Discounts\Domain\Actions\AddOrderItemValueDiscount;
use Jkeppens\Discounts\Domain\Actions\AddOrderPercentageDiscount;
use Jkeppens\Discounts\Domain\Actions\AddOrderValueDiscount;

return [
    [
        'name' => 'add-free-items',
        'class' => AddFreeItems::class,
        'params' => [
            'productId' => [
                'required' => true,
                'type' => 'int',
            ],
            'quantity' => [
                'required' => true,
                'type' => 'int',
            ],
            'quantityPerChunkOf' => [
                'required' => false,
                'type' => 'int',
            ],
        ],
    ],
    [
        'name' => 'add-order-item-percentage-discount',
        'class' => AddOrderItemPercentageDiscount::class,
        'params' => [
            'percentage' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'add-order-item-value-discount',
        'class' => AddOrderItemValueDiscount::class,
        'params' => [
            'discount' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'add-order-percentage-discount',
        'class' => AddOrderPercentageDiscount::class,
        'params' => [
            'percentage' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'add-order-value-discount',
        'class' => AddOrderValueDiscount::class,
        'params' => [
            'discount' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ],
    [
        'name' => 'add-order-item-percentage-discount-on-one-of-the-items',
        'class' => AddOrderItemPercentageDiscountOnOneOfTheItems::class,
        'params' => [
            'checkOn' => [
                'required' => true,
                'type' => 'string',
                'values' => ['unit_price', 'total', 'quantity'],
            ],
            'checkFor' => [
                'required' => true,
                'type' => 'string',
                'values' => ['min', 'max'],
            ],
            'percentage' => [
                'required' => true,
                'type' => 'float',
            ],
        ],
    ]
];